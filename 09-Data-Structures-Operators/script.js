'use strict';

// // Data needed for a later exercise
// const flights =
//   '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

// // Data needed for first part of the section
// const restaurant = {
//   name: 'Classico Italiano',
//   location: 'Via Angelo Tavanti 23, Firenze, Italy',
//   categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
//   starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
//   mainMenu: ['Pizza', 'Pasta', 'Risotto'],

//   openingHours: {
//     thu: {
//       open: 12,
//       close: 22,
//     },
//     fri: {
//       open: 11,
//       close: 23,
//     },
//     sat: {
//       open: 0, // Open 24 hours
//       close: 24,
//     },
//   },
//   order: function (starterIndex, mainIndex) {
//     return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
//   },
//   orderDelievery: function ({
//     starterIndex = 1,
//     mainIndex = 0,
//     time = '20:00',
//     address,
//   }) {
//     console.log(
//       `Order Recieved!! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]} will be delievered to '${address}', at ${time}`
//     );
//   },
//   orderPasta: function (ing1, ing2, ing3) {
//     console.log(
//       `Here is your delicious pasta with '${ing1}', '${ing2}' and '${ing3}'`
//     );
//   },
//   orderPizza: function (mainIng, ...otherIng) {
//     console.log(
//       `The pizza consit of '${mainIng}' toppings and other ingredients as '${otherIng}'.`
//     );
//   },
// };

/* ARRAY DESTRUCTURING */

/* Example of Destructuring */
// const arr = [2, 3, 4];
// const a = arr[0];
// const b = arr[1];
// const c = arr[2];
// console.log(a, b, c);
// const [x, y, z] = arr;
// console.log(y);

// let [main, , , secondary] = restaurant.categories;
// console.log(main, secondary);
/* Switching without destructuring */
// let temp = main;
// main = secondary;
// secondary = temp;

/* Switching with destructuring */

// [main, secondary] = [secondary, main];
// console.log(main, secondary);

// const [starterOrder, mainOrder] = restaurant.order(2, 0);
// console.log(starterOrder, mainOrder);

/* Destructuring For Nested Array */
// const empInfo = [
//   'Ajay',
//   'Kumar',
//   1993,
//   30,
//   ['Vijay', 'Komal', 'Ravi', 'Soumya'],
// ];
// const [firstName, lastName, birthYear, age, friends] = empInfo;

// console.log(friends);
// console.log(lastName);

// const [_firstName, , , , [, , friend]] = empInfo;
// console.log(_firstName);
// console.log(friend);

/* Default values in Array Destructuring */
// const arr1 = [8, 9];
// Without default values
// const [p, q, r] = arr1;
// console.log(p, q, r); // Here 'r' will be undefined.
// const [p = 1, q = 1, r = 1] = arr1;
// console.log(p, q, r); // Here the value for the 'r' will be 1 by default.

/* OBJECTS DESTRUCTURING */

/* Destructuring with only property Names of object */
// const { name, openingHours, categories } = restaurant;
// console.log(name, openingHours, categories);

/* Destructuring with only variable Names */
// const {
//   name: restaurantName,
//   openingHours: hours,
//   categories: tags,
// } = restaurant;

// console.log(restaurantName, hours, tags);

/* Setting the default values if the property does'nt exist */
// const { menu = [], starterMenu: starters = [] } = restaurant;
// console.log(starters);

/* Mutating the variables in the Object Destructuring */
// let a = 111;
// let b = 999;
// const obj = { a: 23, b: 7, c: 14 };
// console.log(a, b); // Initial values of 'a' and 'b'.
// {a,b}=obj; // Here we can't directly re-assign the values of 'a' and 'b'.
// ({ a, b } = obj);
// console.log(a, b); // Mutated values of 'a' and 'b'.

/* Nested Objects Destructuring */

// const { mon = 'Not-Opened', sat: satTimings = 'Not-Opened' } = openingHours;
// console.log(satTimings);
// console.log(mon);

// const {
//   fri: { open: openTimings, close: closeTimings },
// } = openingHours;

// console.log(openTimings, closeTimings);

// Passing the object as the parameters to the function
// restaurant.orderDelievery({
//   time: '22:30',
//   address: 'Via del sole,21',
//   mainIndex: 2,
//   starterIndex: 2,
// });

// restaurant.orderDelievery({
//   address: 'Via del sole,21',
//   mainIndex: 2,
// });

/* THE SPREAD OPERATOR */

// const arr = [7, 8, 9];

/* Creating new array with Adding new values at starting. */
// const badNewArr = [1, 2, arr[0], arr[1], arr[2]];
// console.log(badNewArr);

/* Same operation in ES6 with spread operator */
// const newArr = [1, 2, ...arr];
// console.log(newArr); //  Log the values as array.
// console.log(...newArr); // Log the values as indivisual numbers.

/* Example for the Restaurant object. */
// const newMenu = ['Chowmins', ...restaurant.mainMenu]; // Adding the "Chowmins" in the restaurants main menu.
// console.log(newMenu);

/* Use cases of spread operators */
// (a) Create Shallow Copy of Array
// const mainMenuCopy = [...restaurant.mainMenu]; // Creating the copy of 'Restaurants' mainMenu copy
// console.log(mainMenuCopy);

// (b) Joining 2 arrays
// const joinArr = [...restaurant.mainMenu, ...restaurant.starterMenu]; // Joining Restaurant's mainMenu and starterMenu.
// console.log(joinArr);

/* (c) Working with strings */
// const a = 'Hello';
// const b = 'World!!';
// const str = [...a, '', ...b];
// console.log(...a);
// console.log(str);

/* (d) Spread example with function */
// const ing = [
//   prompt("Let's make pasta! Ingredient1?"),
//   prompt("Let's make pasta! Ingredient2?"),
//   prompt("Let's make pasta! Ingredient3?"),
// ];

// restaurant.orderPasta(...ing);

/* (e) Working with objects */

// Creating new restaurant with some additional info
// const newRestaurant = {
//   foundedIn: '1991',
//   founder: 'Ajay Kumar',
//   country: 'India',
//   ...restaurant,
// };
// console.log(newRestaurant);

// Copying restaurant properties to new restaurants and changed some existing property values.
// const restCopy = { ...restaurant };
// restCopy.name = 'Ajay da dhabba';
// console.log(restCopy);

/* REST PATTERN AND REST PARAMETERS */

/* Rest for arrays */

// const arr = [1, 2, ...[3, 4, 5]];
// console.log(arr);

// const [a, b, ...others] = [2, 3, 4, 5, 6, 7];

// console.log(a, b, others);

// const [pizza, , risotto, ...other] = [
//   ...restaurant.mainMenu,
//   ...restaurant.starterMenu,
// ];

// console.log(pizza);
// console.log(risotto);
// console.log(other);

/* Rest for objects */

// const { sat: weekends, ...weekdays } = restaurant.openingHours;

// console.log(weekends);

// console.log(weekdays);

/* Rest example with function */
// let temp;
// const add = function (...arr) {
//   temp = arr[0];
//   for (let i = 1; i < arr.length; i++) {
//     temp += arr[i];
//   }
//   // console.log(arr);
//   console.log(temp);
// };

// add(2, 3);
// add(4, 3, 3, 1);
// add(5, 3, 2, 5, 6, 1);

// const x = [23, 11, 2];
// // console.log(...x);
// add(...x);

/* Rest example with restaurant object */
// restaurant.orderPizza('Onion');
// restaurant.orderPizza('Mushrooms', 'Oregano', 'salt', 'capsicum', 'cheese');

/* SHORT CIRCUITING (&& and ||) */

/* || */
// If the first value is 'truthy' value then js will not look/evaluate the second value.
// Only if the first value is 'falsy' value then the second value get evaluated.
// console.log(3 || 'Ajay'); // truthy || truthy => 3
// console.log('' || 'Ajay'); // falsy || truthy => Ajay
// console.log(true || 0); // truthy || falsy => true
// console.log(false || true); // falsy || truthy => true
// console.log(undefined || null); // falsy || falsy => null
// console.log(null || undefined); // falsy || falsy => undefined
// console.log(null || undefined || '' || 0 || 'Hello' || 23 || 'Welcome'); // falsy || falsy || falsy || falsy || truthy => Hello

/* Examples on restaurant */

// // Way-1
// const guest1 = restaurant.gestValue ? restaurant.gestValue : 10;
// console.log(guest1);

// // Way-2 Similar to logic way-1
// const guest2 = restaurant.gestValue || 13; // falsy || truthy => 13
// console.log(guest2);

// restaurant.gestValue = 254;
// const guest3 = restaurant.gestValue || 13; // truthy || falsy => 23
// console.log(guest3);

/* && */
// If the first value is 'truthy' value then only the second value will be evaluated.
// But if the first value is 'falsy' value then the second value will not get evaluated.
// console.log(3 && 'Ajay'); // truthy && truthy => Ajay
// console.log('' && 'Ajay'); // falsy && truthy => ''
// console.log(true && 0); // truthy && falsy => 0
// console.log(false && true); // falsy && truthy => false
// console.log(undefined && null); // falsy && falsy => undefined
// console.log(null && undefined); // falsy && falsy => null
// console.log(null && undefined && '' && 0 && 'Hello' && 23 && 'Welcome'); // falsy && falsy && falsy && falsy && truthy => null
// console.log('Hello' && 'Welcome' && 23 && 0 && 'Hello' && 23 && 'Welcome'); // falsy && falsy && falsy && falsy && truthy => 0

// Restaurant Example

// // Way-1
// if (restaurant.orderPizza) {
//   restaurant.orderPizza('Mushrooms', 'Onions', 'Cheese');
// }

// // Way-2 Similar to logic way-1
// restaurant.orderPizza && restaurant.orderPizza('Mushrooms', 'Onions', 'Cheese');

/* THE NULLISH COALESCING OPERATOR (??) */

// console.log(3 ?? 'Ajay'); // truthy && truthy => Ajay
// console.log('' ?? 'Ajay'); // falsy && truthy => ''
// console.log(true ?? 0); // truthy && falsy => 0
// console.log(false ?? true); // falsy && truthy => false
// console.log(undefined ?? null); // falsy && falsy => undefined

// /* Examples on restaurant */

// // Way-1 with '||' operator
// restaurant.gestValue = 0; // Here '0' is not 'nullish or undefined' value.
// const guest1 = restaurant.gestValue || 13; // Here get output as =>13, bcoz '0' is falsy value.
// console.log(guest1);

// // Way-2 with '&&' operator
// const guest2 = restaurant.gestValue ?? 13; // 0, bcoz '0' is not 'nullish or undefined' value.
// console.log(guest2);

/* LOGICAL ASSIGNMENT OPERATOR */

// const rest1 = {
//   name: 'Taj',
//   numGuests: 0,
// };

// const rest2 = {
//   name: 'Oberoi',
//   owner: 'Ajay Kumar',
// };

// // Way-1, setting the default value of 'numGuests' as 10 if don't have any.
// rest1.numGuests = rest1.numGuests || 10;
// rest2.numGuests = rest2.numGuests || 10;

// // Logical OR Operator
// rest1.numGuests ||= 10;
// rest2.numGuests ||= 10;

// // Logical Nullish operator
// rest1.numGuests ??= 10;
// rest2.numGuests ??= 10;

// // Logical AND operator
// // rest2.owner = rest2.owner && '<Anonymous>'; // Same code works if the below code is written
// rest2.owner &&= '<Anonymous>'; // work similary as of the obove code
// rest1.owner &&= '<Anonymous>';

// console.log(rest1);
// console.log(rest2);

/* LOOPING ARRAYS: THE For-oF LOOP */

// const menu = [...restaurant.starterMenu, ...restaurant.mainMenu];

// for (const item of menu) {
//   console.log(item);
// }

// // To get the indexes of the array in the 'for-of' loop.
// for (const item of menu.entries()) {
//   console.log(item);
// }

// // console.log([...menu.entries()]);

// for (const [i, j] of menu.entries()) {
//   console.log(`${i + 1}: ${j}`);
// }

/* ENHANCED OBJECT LITERALS */

// const weekDays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
// const openingHours = {
//   // ES6 version of writing the property names.
//   [weekDays[3]]: {
//     open: 12,
//     close: 22,
//   },
//   fri: {
//     open: 11,
//     close: 23,
//   },
//   sat: {
//     open: 0, // Open 24 hours
//     close: 24,
//   },
// };

// const locations = ['location-1', 'location-2', 'location-3', 'location-4'];

// const restaurant1 = {
//   name: 'Classico Italiano',

//   // ES6 version of writing the property name
//   [locations[1]]: 'Via Angelo Tavanti 23, Firenze, Italy',

//   categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
//   starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
//   mainMenu: ['Pizza', 'Pasta', 'Risotto'],

//   // ES6 version to write object inside the object.
//   openingHours,

//   // ES6 version to write methods inside the object.
//   order(starterIndex, mainIndex) {
//     return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
//   },
//   orderDelievery({ starterIndex = 1, mainIndex = 0, time = '20:00', address }) {
//     console.log(
//       `Order Recieved!! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]} will be delievered to '${address}', at ${time}`
//     );
//   },
//   orderPasta(ing1, ing2, ing3) {
//     console.log(
//       `Here is your delicious pasta with '${ing1}', '${ing2}' and '${ing3}'`
//     );
//   },
//   orderPizza(mainIng, ...otherIng) {
//     console.log(
//       `The pizza consit of '${mainIng}' toppings and other ingredients as '${otherIng}'.`
//     );
//   },
// };

// console.log(restaurant1);
// restaurant1.orderPizza('Tomato', 'Oregano');

/* OPTIONAL CHAINING (?.) */

// // Restaurant Example

// const restaurant1 = {
//   name: 'Kake da dhabba',
//   openingHours: {
//     wed: {
//       open: '08',
//       close: '12',
//     },
//     thu: {
//       open: '12',
//       close: '23',
//     },
//     sat: {
//       open: 0,
//       close: 12 + 12,
//     },
//   },
//   mainMenu: ['Pizza', 'Pasta', 'Risotto'],

//   order(mainIndex) {
//     return `You order the delicious "${this.mainMenu[mainIndex]}"`;
//   },
// };

// // Way-1
//  console.log(restaurant1.openingHours.mon); // Gives output as 'Undefined' bcoz 'mon' property doesn't exist in object.
//  console.log(restaurant1.openingHours.mon.open); // Gives an error on "Can't read property of undefined" bcoz property 'mon' is not defined.

// // Way-2 // It gives an undefined value as output
// console.log(
//   restaurant1.openingHours &&
//     restaurant1.openingHours.mon &&
//     restaurant1.openingHours.mon.open
// );

// // Way-3 // It gives an undefined value as output
// if (restaurant1.openingHours && restaurant1.openingHours.mon) {
//   console.log(restaurant1.openingHours.mon.open);
// }

// // Way-4 // Optional Chaining (?.)
// console.log(restaurant1.openingHours?.mon?.open);

// // Real world Example of optional chaining

// const days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

// // Printing the values of 'open' for all the days
// for (const day of days) {
//   const open = restaurant1.openingHours[day]?.open ?? '08';
//   console.log(`On ${day}, we open at ${open}`);
// }

// // Calling methods using 'Optional Chaining'.
// console.log(restaurant1.order?.(2) ?? `Method doesn't exist`);
// console.log(restaurant1.orderPizza?.(2) ?? `No such functionality exist`);

// // Arrays with "Optional Chaining"
// const myInfo = [
//   {
//     firstName: 'Ajay',
//     lastName: 'Kumar',
//     birthYear: 1993,
//   },
// ];
// console.log(myInfo);
// console.log(myInfo[0]?.firstName ?? 'Not Exist');
// console.log(myInfo[1]?.firstName ?? 'Not Exist');

/* LOOPING OBJECTS: OBJECT KEYS, VALUES, ENTRIES */

// const restaurant1 = {
//   name: 'Kake da dhabba',
//   openingHours: {
//     wed: {
//       open: '08',
//       close: '12',
//     },
//     thu: {
//       open: '12',
//       close: '23',
//     },
//     sat: {
//       open: 0,
//       close: 12 + 12,
//     },
//   },
//   mainMenu: ['Pizza', 'Pasta', 'Risotto'],

//   order(mainIndex) {
//     return `You order the delicious "${this.mainMenu[mainIndex]}"`;
//   },
// };

// (1) Looping over property names (Keys)

// for (const day of Object.keys(restaurant1.openingHours)) {
//   console.log(day);
// }
// const properties = Object.keys(restaurant.openingHours);
// const totalDays = properties.length;
// let str1 = `We are open on ${totalDays} days :`;

// console.log(properties);
// // Way-1 of printing
// console.log(str1 + `${[...properties]}`);

// // Way-2 of printing
// for (const day of properties) {
//   str1 += `${day},`;
// }
// console.log(str1);

// // (2) Looping over properties values (Values)

// const values = Object.values(restaurant1.openingHours);
// // console.log(values);
// const entries = Object.entries(restaurant1.openingHours);

// for (const [key, { open: opening, close: closing }] of entries) {
//   console.log(
//     `On '${key} we open at '${opening}am' and close at '${closing}pm'`
//   );
// }

/* SETS */

// // Array
// const arr = ['pizza', 'pasta', 'maggie', 'pizza', 'coffee', 'pasta']; // It will keep the duplicate data.
// console.log(arr);

// // Create Set
// const name = new Set('Ajay');
// console.log(name);
// const birthYear = new Set('1993');
// console.log(birthYear);
// const set1 = new Set(['pizza', 'pasta', 'maggie', 'pizza', 'coffee', 'pasta']); // It will not keep the duplicate data.
// console.log(set1);

// // Get size of set
// console.log(set1.size); // Return the length of set

// // Check for any element in set
// console.log(set1.has('pizza')); // Return the boolean value

// // Add new element into the set
// set1.add('Bread');
// console.log(set1);

// // Retrieving element from sets
// console.log(set1[1]); // It gives undefined.

// // Delete elements from the set
// set1.delete('Bread');
// console.log(set1);
// set1.delete('maggie');
// console.log(set1);

// // Delete all the element from the sets
// set1.clear();
// console.log(set1);

// // Iterate over sets
// const set2 = new Set(['pizza', 'pasta', 'maggie', 'pizza', 'coffee', 'pasta']);
// for (const order of set2) {
//   console.log(order);
// }

// //Example of sets
// const staff = ['waiter', 'chef', 'waiter', 'manager', 'chef', 'waiter'];
// const set3 = new Set(staff);
// console.log(set3);

// // To convert the set into array using the spread method
// const set4 = [...new Set(staff)];
// console.log(set4);
// // Get the number of the unique elements in array using set
// console.log(new Set(staff).size);
// console.log(new Set('Ajay Kumar').size);

/* MAPS FUNDAMETALS*/

// // Create map.
// const hotel = new Map();

// // Set values in map.
// hotel.set('Name', 'Radisson');
// hotel.set(1, 'America');
// hotel.set(2, 'India');
// console.log(hotel.set(3, 'South Africa'));
// hotel.set('open', 12);
// hotel.set('close', 23);
// hotel.set(true, 'We are open:)');
// hotel.set(false, 'We are close(:');
// hotel.set('categories', ['Italian', 'Pizzeria', 'Vegetarian', 'Organic']);

// // Setting multiple values.
// hotel
//   .set('Breads', 'Aaloo Naan')
//   .set('Rice', 'jeera rice')
//   .set('mainCourse', 'Veg Thali');

// // Display all data
// console.log(hotel);

// // Read particular data from map
// console.log(hotel.get('Name'));
// console.log(hotel.get('categories'));

// // In the example to check wether the hotel is open or not
// const time = 9;

// // Way-1: By directly using the map.get property
// console.log(hotel.get(time > hotel.get('open') && time < hotel.get('close')));

// // Way-2: By checking with if else condition directly
// console.log(
//   time > hotel.get('open') && time < hotel.get('close')
//     ? hotel.get(true)
//     : hotel.get(false)
// );

// // Way-3: By Create function to check the hotel opening and closing timings
// function printValid(time, openDate, closeDate) {
//   if (time > openDate && time < closeDate) {
//     return hotel.get(true);
//   } else {
//     return hotel.get(false);
//   }
// }
// console.log(printValid(time, hotel.get('open'), hotel.get('close'))); // Calling function

// // Has() method with map to check for particular property
// console.log(hotel.has('categories'));

// // Delete the element from map
// hotel.delete('categories');
// console.log(hotel);

// // Get Size of map
// console.log(hotel.size);

// // Setting array as keys
// const arr = [1, 2];
// hotel.set(arr, 'Test');
// console.log(hotel.get(arr));

// // Setting DOM objects as key
// console.log(hotel.set(document.querySelector('h1'), 'Heading'));

// // Delete all the elements from the map
// hotel.clear();
// console.log(hotel);

/* MAPS ITERATION */

// // Second way of creating the maps without "Set()"
// const question = new Map([
//   ['Question', 'What is my favourite language ?'],
//   [1, 'Java'],
//   [2, 'C++'],
//   [3, 'JavaScript'],
//   [4, 'Python'],
//   ['Correct', 3],
//   [true, 'Correct ✔'],
//   [false, 'Try Again ❌'],
// ]);

// // Convert Object to Map
// const openingHours = {
//   thu: {
//     open: 12,
//     close: 22,
//   },
//   fri: {
//     open: 11,
//     close: 23,
//   },
//   sat: {
//     open: 0, // Open 24 hours
//     close: 24,
//   },
// };

// const hours = new Map(Object.entries(openingHours));
// console.log(hours);

// // Map iteration
// console.log(question.get('Question'));
// for (const [key, value] of question) {
//   if (typeof key === 'number') {
//     console.log(`Answer ${key}: ${value}`);
//   }
// }

// // Printing question and Taking answer from the user
// const answer = Number(
//   prompt(
//     `${question.get('Question')}\n\nAnswers:-\n(1.) ${question.get(
//       1
//     )}\n(2.) ${question.get(2)}\n(3.) ${question.get(3)}\n(4.) ${question.get(
//       4
//     )}\n\nKindly select your answer`
//   )
// );

// // Way-1: Checking the user answer and printing the result
// console.log(
//   question.get('Correct') === answer ? question.get(true) : question.get(false)
// );

// // Way-2: Checking the user answer and printing the result
// for (const [key, value] of question) {
//   if (key === 'Correct') {
//     if (answer === value) {
//       console.log(`${question.get(true)}`);
//     } else {
//       console.log(`${question.get(false)}`);
//     }
//   }
// }

// // Converting maps to array
// const [...newMapArr] = question;
// console.log(newMapArr);
// console.log(typeof newMapArr);

// // Method we can use in maps.
// // Method-1 => entries()
// console.log([...question.entries()]);
// // Method-2 => keys()
// console.log([...question.keys()]);
// // Method-3 => values()
// console.log([...question.values()]);

/* WHICH DATA STRUCTURES TO USE */

/* Sources of data:-
(1) From the program itself => Data written by the programmer directly in the source code.
(2) From the webpage/UI => Data input from the user or the data written in the DOM.
(3) From the external sources => Mostly from the API's.*/

/* WORKING WITH STRING PART-1 */

const airline = 'TAP Air India';
const plane = 'A320';
// console.log(plane); // "A320"

// // Printing the characters of the string using index.
// console.log(plane[1]); // "A"
// console.log('A320'[0]); // "A"

// // Length of strings.
// console.log(airline.length); // "13"
// console.log('TAP Air India'.length); // "13"

// // Methods of strings.
// //(1) indexOf() method => To know the first occured index of any particular character in a given string.
// console.log(airline.indexOf('P')); // "2"
// console.log('Ajay Kumar'.indexOf('K')); // "5"
// console.log(airline.indexOf('Air')); // Gives the index from where the given word starts.

// //(2) lastIndexOf() method => To know the last occured index of any particular character in a given string.
// console.log(airline.lastIndexOf('i')); // "11"
// console.log('Ajay Kumar'.lastIndexOf('a')); // "8"

// //(3) slice() method => To extract the given string from the given index as parameter.
// // We can give "starting" and "last" indexes.
// console.log(airline.slice(4)); // "Air India"
// console.log(airline.slice(4, 7)); // "Air"

// // Real time example of Slice() and indexOf() methods.

// // Slicing from front
// const wordFirstIndex = airline.lastIndexOf('A'); // "4"
// const wordLastIndex = airline.lastIndexOf('r') + 1; // "7"
// console.log(airline.slice(wordFirstIndex, wordLastIndex + 1)); // "Air"
// console.log(airline.slice(0, airline.lastIndexOf(' '))); // "TAP Air"
// console.log(airline.slice(airline.indexOf('T'), airline.indexOf(' '))); // "TAP"
// console.log(airline.slice(airline.lastIndexOf('A'), airline.lastIndexOf(' '))); // "Air"
// console.log(airline.slice(airline.lastIndexOf(' ') + 1)); // "India"

// // Slicing from back
// console.log(airline.slice(-4));
// console.log(airline.slice(1, -1));

// // Real world example
// const checkMiddleSeat = function (seat) {
//   // B and E are the middle seats
//   const lastChar = seat.slice(-1);
//   if (lastChar === 'B' || lastChar === 'E') {
//     console.log('Yes you have middle seat.');
//   } else {
//     console.log("Sorry you don't have middle seat");
//   }
// };
// checkMiddleSeat('11E');

// //  toLowerCase() method
// console.log(airline.toLowerCase());

// // toUpperCase() method
// console.log(airline.toUpperCase());

// // Fix the capitalization name
// const passanger = 'JoNaS'; // Change the given string to "Jonas"
// const passangerLower = passanger.toLowerCase(); // jonas
// const passangerCorrect = passanger[0].toUpperCase() + passangerLower.slice(1); // Jonas
// console.log(passangerCorrect);

// // Function to conver the string into camel case
// function correctString(str) {
//   const strLower = str.toLowerCase();
//   return `${strLower[0].toUpperCase()}${strLower.slice(1)}`;
// }

// console.log(correctString('AjaY'));

// // Comparing Emails
// const email = 'hello@jonas.io'; // User's actual email.
// const loginEmail = '  Hello@Jonas.Io '; // User enter this email at login page
// // Way-1 By creating variables for each steps
// const emailLower = loginEmail.toLowerCase();
// // Trimming
// const trimmedEmail = emailLower.trim();
// console.log(emailLower);
// console.log(trimmedEmail);

// // Way-2 By Lowering and Trimming in the single line
// const normalizedEmail = loginEmail.toLowerCase().trim();
// console.log(
//   loginEmail.toLowerCase().trim() === email ? `Same Email` : `Not Same`
// );

// console.log(normalizedEmail === email ? `Same Email` : `Not Same Email`);

// // Way-3 By creating function for comparing the email
// function compareEmail(actualEmail, emailInput) {
//   const normalizedEmail = emailInput.toLowerCase().trim();
//   if (actualEmail === normalizedEmail) {
//     return `Valid Email`;
//   } else {
//     return `InValid Email`;
//   }
// }
// console.log(compareEmail('ak2681993@gmail.com', '  AK2681983@Gmail.Com  '));

// // Replace the parts of the string
// const priceGB = '288,39$';
// const priceIND = priceGB.replace('$', 'Rs').replace(',', '.');
// console.log(priceIND);

// const announcement =
//   'All passangers come to boarding door 23, boarding door 23!';
// console.log(`Before replacing :- ${announcement}`);

// // Replace single word/Characters with "replace()"
// console.log(
//   `With "replace()" method :- ${announcement.replace('door', 'gate')}`
// );
// // Replace multiple same word/Characters with "replaceAll()"
// console.log(
//   `With "replaceAll()" method :- ${announcement.replaceAll('door', 'gate')}`
// );
// // Replacing the all characters/words with use of "Regular Expression"
// console.log(
//   `With "Regular Expression :- ${announcement.replace(/door/g, 'gate')}`
// );

// // includes() method :- To check the presence of the given character/word in the String
// const plane1 = 'A320neo';
// console.log(plane1.includes('A320')); // true

// // startsWith() method :- To check wether the string is starting with given character or not
// console.log(plane1.startsWith('A3')); // false

// // endsWith() method :- To check wether the string is ending with given character or not
// console.log(plane1.endsWith('o')); // true

// // Practice Code
// const checkBaggage = function (items) {
//   if (
//     items.toLowerCase().includes('knife') ||
//     items.toLowerCase().includes('gun')
//   ) {
//     console.log('You are not allowed on board!!');
//   } else {
//     console.log('Welcome on board!!');
//   }
// };
// checkBaggage('I have a laptop, some food and a camera.');
// checkBaggage('I have some candies and a gun for protection.');
// checkBaggage('I have some food and a pocket Knife.');

// // Split() and join() methods
// console.log('A+very+nice+person');
// console.log('A+very+nice+person'.split('+'));
// console.log('A+very+nice+person'.split('nice'));

// const [firstName, lastName] = 'Ajay Kumar'.split(' ');
// console.log(firstName);
// console.log(lastName);

// console.log(['Mr.', firstName, lastName.toUpperCase()].join(' '));

// // Example
// const passanger = 'jessica ann smith davis';

// function capitalize(name) {
//   const names = name.split(' ');
//   console.log(names);
//   const upper = [];

//   for (const n of names) {
//     // First Way
//     upper.push(n[0].toUpperCase() + n.slice(1));

//     // // Second Way
//     // upper.push(n.replace(n[0], n[0].toUpperCase()));
//   }
//   console.log(upper.join(' '));
// }
// capitalize(passanger);

// // Padding a string => Adding letters until a desired length acheived.
// const message = 'Go to gate 23!!';
// // padStart() method
// console.log(message.padStart(25, '+'));
// // padEnd() method
// console.log(message.padEnd(25, '+'));

// // Example
// const atmCardNumber = '982789467389';
// console.log(atmCardNumber);

// // Function to mask the atm card number with *'s
// const maskCreditCardNum = function (cardnum) {
//   const toString = String(cardnum);
//   return toString.slice(-4).padStart(toString.length, '*');
// };
// console.log(maskCreditCardNum(atmCardNumber));
// console.log(maskCreditCardNum('12432389756467'));

// // Repeat() method => to repeat the strings multiple times.
// const message1 = 'Bad wether......All departures delayed.....';
// console.log(message1.repeat(2));

// const planesInLine = function (n) {
//   console.log(`Their are ${n} planes in line ${'🛫'.repeat(n)}`);
// };

// planesInLine(10);
// planesInLine(13);
// planesInLine(5);

// Strings Practice
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

const getCode = str => str.slice(0, 3).toUpperCase();
for (const flight of flights.split('+')) {
  const [type, from, to, time] = flight.split(';');
  const output = `${type.startsWith('_Delayed') ? '🔴' : ''} ${type.replaceAll(
    '_',
    ''
  )} from ${getCode(from)} to ${getCode(to)} (${time.replace(
    ':',
    'h'
  )})`.padStart(47);
  console.log(output);
}
