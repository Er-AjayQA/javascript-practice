'use strict';

// SELECTING ELEMENTS

const player0EL = document.querySelector('.player--0');
const player1EL = document.querySelector('.player--1');
const score0EL = document.querySelector('#score--0');
const score1EL = document.getElementById('score--1');
const diceEL = document.querySelector('.dice');
const btnNew = document.querySelector('.btn--new');
const btnRoll = document.querySelector('.btn--roll');
const btnHold = document.querySelector('.btn--hold');
const currentScore0EL = document.getElementById('current--0');
const currentScore1EL = document.getElementById('current--1');

// VARIABLES
score0EL.textContent = 0;
score1EL.textContent = 0;
let scores, currentScore, activePlayer, playing;

// FUNCTIONS

// Hide Elements Function
function hideDice() {
  diceEL.classList.add('hidden');
}
function showDice() {
  diceEL.classList.remove('hidden');
}

const switchPlayer = function () {
  document.getElementById(`current--${activePlayer}`).textContent = 0;
  currentScore = 0;
  activePlayer = activePlayer === 0 ? 1 : 0;
  player0EL.classList.toggle('player--active');
  player1EL.classList.toggle('player--active');
};

const init = function () {
  hideDice();
  currentScore = 0;
  activePlayer = 0;
  playing = true;
  scores = [0, 0];
  score0EL.textContent = 0;
  score1EL.textContent = 0;
  currentScore0EL.textContent = currentScore;
  currentScore1EL.textContent = currentScore;
  player0EL.classList.add('player--active');
  player1EL.classList.remove('player--active');
  player0EL.classList.remove('player--winner', 'name');
  player1EL.classList.remove('player--winner', 'name');
};

// FUNCTIONALITY

// Hidding the dice functionality
hideDice();

// Rolling dice functionality
btnRoll.addEventListener('click', function () {
  if (playing) {
    // 1. Generating the random number.
    const diceNumber = Math.trunc(Math.random() * 6) + 1;
    console.log(diceNumber);

    // 2. Display the number.
    showDice();
    diceEL.src = `dice-${diceNumber}.png`;

    // 3. Check for roll 1.
    if (diceNumber !== 1) {
      // Add numbers to current score.
      currentScore += diceNumber;
      document.getElementById(`current--${activePlayer}`).textContent =
        currentScore;
    } else {
      // Switch to next player.
      switchPlayer();
    }
  }
});

// Functionality of Hold button
btnHold.addEventListener('click', function () {
  if (playing) {
    // 1. Add current score to user's total score.
    scores[activePlayer] += currentScore;
    document.getElementById(`score--${activePlayer}`).textContent =
      scores[activePlayer];

    // 2. Check the total score of the player is >=100.
    // If yes then player wins.
    if (scores[activePlayer] >= 100) {
      // Adding Winner class
      playing = false;
      document
        .querySelector(`.player--${activePlayer}`)
        .classList.add('player--winner', 'name');
      // Removing Active class
      document
        .querySelector(`.player--${activePlayer}`)
        .classList.remove('player--active');
      hideDice();
      btnRoll.ariaDisabled;
      btnHold.ariaDisabled;
    } else {
      // Else switch to the next player.
      switchPlayer();
    }
  }
});

// Functionality of New Button
btnNew.addEventListener('click', init);
