'use strict';

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data
const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
};

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
};

const account3 = {
  owner: 'Steven Thomas Williams',
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
};

const account4 = {
  owner: 'Sarah Smith',
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
};

const accounts = [account1, account2, account3, account4];

// Elements
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const labelSumInterest = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');

/////////////////////////////////////////////////
//  Implement Display Movements
const displayMovements = function (movements, sort = false) {
  containerMovements.innerHTML = '';

  const movs = sort ? movements.slice().sort((a, b) => a - b) : movements;

  movs.forEach(function (mov, i) {
    const type = mov > 0 ? 'deposit' : 'withdrawal';

    const html = `
    <div class="movements__row">
    <div class="movements__type movements__type--${type}">${i + 1} ${type}</div>
    <div class="movements__value">${mov}</div>
    </div>`;
    containerMovements.insertAdjacentHTML('afterbegin', html);
  });
};

/////////////////////////////////////////////////
//  Implement Creating and Display UserNames
const createUserNames = function (accs) {
  accs.forEach(function (acc) {
    acc.userName = acc.owner
      .toLowerCase()
      .split(' ')
      .map(name => name[0])
      .join('');
  });
};

createUserNames(accounts);

/////////////////////////////////////////////////
//  Implement Calculating and Display the Current Balance
const calcDisplayBalance = function (acc) {
  acc.balance = acc.movements.reduce((accu, mov) => accu + mov, 0);

  labelBalance.textContent = `${acc.balance} EUR`;
};

/////////////////////////////////////////////////
//  Implement Calculating Statics/ Summary

const calcDisplaySummary = function (acc) {
  const incomes = acc.movements
    .filter(mov => mov > 0)
    .reduce((acc, mov) => acc + mov, 0);
  labelSumIn.textContent = `${incomes}€`;

  const outcomes = acc.movements
    .filter(mov => mov < 0)
    .reduce((acc, mov) => acc + mov, 0);

  labelSumOut.textContent = `${Math.abs(outcomes)}€`;

  const interest = acc.movements
    .filter(mov => mov > 0)
    .map(deposit => (acc.interestRate / 100) * deposit)
    .filter((int, i, arr) => {
      return int >= 1;
    })
    .reduce((acc, int) => acc + int, 0);
  labelSumInterest.textContent = `${interest}€`;
};

/////////////////////////////////////////////////
//  Implementing Login Feature

const updateUI = function (acc) {
  // Display Movements
  displayMovements(acc.movements);

  // Display Balance
  calcDisplayBalance(acc);

  // Display Summary
  calcDisplaySummary(acc);
};

// Event Handlers
let currentAccount;
btnLogin.addEventListener('click', function (e) {
  e.preventDefault();

  currentAccount = accounts.find(
    acc => acc.userName === inputLoginUsername.value
  );

  if (currentAccount?.pin === Number(inputLoginPin.value)) {
    // Display UI and Welcome Message
    labelWelcome.textContent = `Welcome back, ${
      currentAccount.owner.split(' ')[0]
    }`;

    containerApp.style.opacity = 1;

    // Clear Input Fields
    inputLoginUsername.value = inputLoginPin.value = '';
    inputLoginPin.blur();

    // Update UI
    updateUI(currentAccount);
  }
});

/////////////////////////////////////////////////
//  Implement Transfer Money

btnTransfer.addEventListener('click', function (e) {
  e.preventDefault();
  const amountTransfer = Number(inputTransferAmount.value);
  const recipentAcc = accounts.find(
    acc => acc.userName === inputTransferTo.value
  );

  // Check the valid transfer
  if (
    amountTransfer > 0 &&
    recipentAcc &&
    currentAccount.balance >= amountTransfer &&
    recipentAcc?.userName !== currentAccount.userName
  ) {
    // Add Positive Movement to Reciepent User
    recipentAcc.movements.push(amountTransfer);
    // Add Negative Movement to Current User
    currentAccount.movements.push(-amountTransfer);
    updateUI(currentAccount);
  }
  inputTransferAmount.value = inputTransferTo.value = '';
});

/////////////////////////////////////////////////
//  Implement Close the Account
btnClose.addEventListener('click', function (e) {
  e.preventDefault();

  if (
    inputCloseUsername.value === currentAccount.userName &&
    currentAccount.pin === Number(inputClosePin.value)
  ) {
    const index = accounts.findIndex(
      acc => acc.userName === currentAccount.userName
    );
    // Delete User
    accounts.splice(index, 1);
    // Hide UI
    containerApp.style.opacity = '0';
  }
  inputCloseUsername.value = inputClosePin.value = '';
});

/////////////////////////////////////////////////
//  Implement Request for the Loan

btnLoan.addEventListener('click', function (e) {
  e.preventDefault();
  const loanAmount = Number(inputLoanAmount.value);

  if (
    loanAmount > 0 &&
    currentAccount.movements.some(mov => mov > 0.1 * loanAmount)
  ) {
    currentAccount.movements.push(loanAmount);
    // Update UI
    updateUI(currentAccount);
  }

  inputLoanAmount.value = '';
});

/////////////////////////////////////////////////
//  OverAll Balance

/* Without Functional Chaining */

// const accountMovements = accounts.map(acc => acc.movements);
// console.log(accountMovements);
// const allMovements = accountMovements.flat();
// console.log(allMovements);

// const overallBalance = allMovements.reduce((acc, mov) => acc + mov, 0);
// console.log(overallBalance);

/* With Functional Chaining */

// const overallBalance = accounts
//   .map(acc => acc.movements)
//   .flat()
//   .reduce((acc, mov) => acc + mov, 0);
// console.log(overallBalance);

/* With flatMap() method */

// // Without functional chaining
// const overallBalance = accounts.flatMap(acc => acc.movements); // Mapping and Flat together
// console.log(overallBalance);

// // With functional chaining
// const overallBalance = accounts
//   .flatMap(acc => acc.movements)
//   .reduce((acc, mov) => acc + mov); // Mapping and Flat together
// console.log(overallBalance);

/////////////////////////////////////////////////
//  Implement Sort Functionality

let sorted = false;

btnSort.addEventListener('click', function (e) {
  e.preventDefault();

  displayMovements(currentAccount.movements, !sorted);
  sorted = !sorted;
});

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// LECTURES

const currencies = new Map([
  ['USD', 'United States dollar'],
  ['EUR', 'Euro'],
  ['GBP', 'Pound sterling'],
]);

/////////////////////////////////////////////////

// const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

/* SIMPLE ARRAY METHODS */

// let arr = ['a', 'b', 'c', 'd', 'e'];

// // Slice() Methods
// console.log(arr.slice(2));
// console.log(arr.slice(-2));
// console.log(arr.slice(1, -2));
// console.log(arr.slice(1, 4));
// console.log(arr);

// // Splice() Methods
// arr = ['a', 'b', 'c', 'd', 'e'];
// // console.log(arr2.splice(2));
// // console.log(arr2.splice(-2));
// // console.log(arr2.splice(1, 3));
// console.log(arr.splice(-1)); // Removing the last element
// console.log(arr);

// // Reverse() Methods
// arr = ['a', 'b', 'c', 'd', 'e'];
// console.log(arr.reverse());
// console.log(arr);

// // Concat() Methods
// arr = ['a', 'b', 'c', 'd', 'e'];
// let arr1 = [1, 2, 3, 4, 5];
// let arr2 = ['Ajay', 'Vijay'];
// console.log(arr.concat(arr1, arr2)); // This is equal to [...arr, ...arr1, ...arr2]
// // console.log([...arr, ...arr1, ...arr2]);
// console.log(arr2.concat(4 * 2));

// // Join() Methods
// arr = ['a', 'b', 'c', 'd', 'e'];
// let arr1 = ['David', 'Smith'];
// console.log(arr.join('-'));
// console.log(arr1.join(' '));
// console.log(arr1.join(' XX===XX '));
// console.log(arr1.join(8));

/* THE NEW AT() METHOD (ES6) */
// const arr = [11, 33, 64, 88, 100];
// // console.log(arr[0]); // Before ES6
// console.log(arr.at(0)); // IN ES6

// // To get last element of array without knowing the length of array
// console.log(arr[arr.length - 1]);
// // To get last element of array using "slice()" method
// console.log(arr.slice(-1)); // Gives the array holding elements from the last.
// console.log(arr.slice(-1)[0]); // To take out the elements from the slice array.
// // To get last element of array using "at()" method
// console.log(arr.at(-1));

// // at() method on Strings
// const str = 'David Smith';
// console.log(str.at(4)); // => d

/* LOOPING ARRAY: FOREACH */

//  const movements = [200, 450, -400, 3000, 0, -650, -130, 70, 1300];
// // PROBLEM:- Print the "Deposit for +ve values" and "Withdraw for -ve values"

// // SOLUTION:- 1 Using "forOf" loop
// console.log('--- USING FOROF ---');
// for (const movement of movements) {
//   if (movement > 0) {
//     console.log(`Deposit: ${movement}`);
//   } else if (movement < 0) {
//     console.log(`Withdrew: ${Math.abs(movement)}`);
//   } else {
//     console.log(`No Transactions!`);
//   }
// }

// // SOLUTION:- 2 Using "forEach" loop
//  console.log('--- USING FOREACH ---');
// movements.forEach(function (movement) {
//   if (movement > 0) {
//     console.log(`Deposit: ${movement}`);
//   } else if (movement < 0) {
//     console.log(`Withdrew: ${Math.abs(movement)}`);
//   } else {
//     console.log(`No Transactions!`);
//   }
// });

// // SOLUTION:- 1.a Using "forOf with Iterator" loop
// console.log('--- forOf with Iterator ---');
// for (const [i, movement] of movements.entries()) {
//   if (movement > 0) {
//     console.log(`(${i + 1}) Deposit: ${movement}`);
//   } else if (movement < 0) {
//     console.log(`(${i + 1}) Withdrew: ${Math.abs(movement)}`);
//   } else {
//     console.log(`(${i + 1}) No Transactions!`);
//   }
// }

// // SOLUTION:- 2.a Using "forEach with Iterator" loop
// console.log('--- forEach with Iterator ---');
// movements.forEach(function (movement, index, array) {
//   if (movement > 0) {
//     console.log(`(${index + 1}) Deposit: ${movement}`);
//   } else if (movement < 0) {
//     console.log(`(${index + 1}) Withdrew: ${Math.abs(movement)}`);
//   } else {
//     console.log(`(${index + 1}) No Transactions!`);
//   }

//   console.log(array);
// });

// // EXAMPLE:- "Break" and "Continue"
// console.log('--- Break With ForOf Loop ---');
// for (const movement of movements) {
//   if (movement >= 2500) {
//     break;
//   } else {
//     console.log(`Good to go!! ${Math.abs(movement)}`);
//   }
// }

// console.log('--- Continue With ForOf Loop ---');
// for (const movement of movements) {
//   if (movement === 0) {
//     continue;
//   } else {
//     console.log(`Good to go!! ${Math.abs(movement)}`);
//   }
// }

// // EXAMPLE:- "Break" and "Continue" with "ForEach"
// console.log('--- "Break" Statement With ForEach Loop ---');
// movements.forEach(function (movement) {
//   if (movement >= 2500) {
//     break; // "Break" statement will not work with "forEach" loop
//   } else {
//     console.log(`Good to go!! ${Math.abs(movement)}`);
//   }
// });

// console.log('--- "Continue" Statement With ForEach Loop ---');
// movements.forEach(function (movement) {
//   if (movement === 0) {
//     continue; // "Continue" statement will not work with "forEach" loop
//   } else {
//     console.log(`Good to go!! ${Math.abs(movement)}`);
//   }
// });

/* FOREACH WITH MAPS AND SETS */

// // Map
// const currencies = new Map([
//   ['USD', 'United States dollar'],
//   ['EUR', 'Euro'],
//   ['GBP', 'Pound sterling'],
// ]);

// currencies.forEach(function (currency, key, map) {
//   console.log(`${key}: ${currency} => ${map}`);
// });

// // Set
// const currenciesUnique = new Set(['USD', 'GBP', 'USD', 'EUR', 'EUR']);
// console.log(currenciesUnique);

// currenciesUnique.forEach(function (values, key, map) {
//   console.log(`${key}: ${values} => ${map}`);
// });

/* CHALLENGE 1 */

// // Data Set 1
// const dogsJulia = [3, 5, 2, 12, 7];
// const dogsKate = [4, 1, 15, 8, 3];

// // Data Set 2
// const dogsJulia = [9, 16, 6, 8, 3];
// const dogsKate = [10, 5, 6, 1, 4];

// const checkDogs = function (dogsJulia, dogsKate) {
//   const juliaObservation = dogsJulia.slice(1, 3);
//   const observations = juliaObservation.concat(dogsKate);
//   console.log(`Final Array Data: ${observations}`);
//   observations.forEach(function (age, i) {
//     console.log(
//       `Dog Number ${i + 1} is ${
//         age >= 3 ? `an adult and is ${age} years old` : 'still a puppy'
//       }`
//     );
//   });
// };

// console.log('FOR TEST DATA 1');
// checkDogs(dogsJulia, dogsKate);

// console.log('FOR TEST DATA 2');
// checkDogs(dogsJulia, dogsKate);

/* MAP, FILTER, REDUCE */

/* MAP METHOD */
// const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
// const euroToUsd = 1.1;

// // Using "MAP" method
// const movementsUSD = movements.map(function (mov) {
//   return mov * euroToUsd;
// });

// const movementsUSD = movements.map(mov => mov * euroToUsd);

// console.log(movementsUSD);
// console.log(movements);

// // Same functionality Using "FOR" loop
// const movArr = [];
// for (const mov of movements) {
//   movArr.push(mov * euroToUsd);
// }
// console.log(movArr);

// // Using "MAP" method parameters
// const movementsDescriptions = movements.map(
//   (mov, i) =>
//     `Movement ${i + 1}: You ${mov > 0 ? 'deposited' : 'withdrew'} ${Math.abs(
//       mov
//     )}`
//   // Same as above
//   // if (mov > 0) {
//   //   return `Movement ${i + 1}: You deposited ${mov}`;
//   // } else {
//   //   return `Movement ${i + 1}: You withdrew ${Math.abs(mov)}`;
//   // }
// );
// console.log(movementsDescriptions);

/* FILTER METHOD */

// const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
// console.log(movements);
// // Using Filter Method
// const deposit1 = movements.filter(mov => mov > 0);
// const withdraw1 = movements.filter(mov => mov < 0);
// console.log(deposit1);
// console.log(withdraw1);

// // Same functionality Using ForOf loop
// const deposit2 = [];
// const withdraw2 = [];
// for (const mov of movements) {
//   if (mov > 0) {
//     deposit2.push(mov);
//   } else {
//     withdraw2.push(mov);
//   }
// }
// console.log(deposit2);
// console.log(withdraw2);

/* REDUCE METHOD */

// const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

// // Using Reduce Method
// const balance1 = movements.reduce(function (acc, mov, i) {
//   console.log(`Iteration ${i}: ${acc}`);
//   return acc + mov;
// }, 0);
// console.log(balance1);

// // Setting Initial value for "Accumulator"
// const balance2 = movements.reduce((acc, mov) => acc + mov, 200);
// console.log(balance2);

// // Same Using the ForOF Loop
// let balance3 = 0;
// for (const mov of movements) {
//   balance3 += mov;
// }
// console.log(balance3);

// // Max Value Using Reduce Method
// const max = movements.reduce(function (acc, mov) {
//   if (acc > mov) {
//     return acc;
//   } else {
//     return mov;
//   }
// }, movements[0]);
// console.log(max);

/* THE MAGIC OF CHAINING */

// const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
// const euroToUsd = 1.1;

// const totalDepositsInUSD = movements
//   .filter(mov => mov > 0)
//   .map((mov, i, arr) => {
//     // console.log(arr);
//     return mov * euroToUsd;
//   })
//   .reduce((acc, mov) => acc + mov, 0);
// console.log(totalDepositsInUSD);

/* CHALLENGE 3 */

// const testData1 = [5, 2, 4, 1, 15, 8, 3];

// const calcAverage = function (data) {
//   data.reduce((acc, age, i) => acc + age, 0);
// };
// console.log(calcAverage(testData1));

/* THE FIND METHOD */

// const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

// const withdraw = movements.find(mov => mov < 0);
// console.log(withdraw);

// const account = accounts.find(acc => acc.owner === 'Jessica Davis');
// console.log(account);

/* THE FINDINDEX METHOD */

// Syntax

/* const index= elementArr.findIndex(function (arg) {
  CSSConditionRule;
}); */

/* SOME AND EVERY METHOD */

// const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

// console.log(movements);
// // Includes Method :- Here we can only test for equality and return the boolean value.
// console.log(movements.includes(456));

// // Some Method :- If we want to test for the condition
// const someDeposit = movements.some(mov => mov > 0);
// console.log(someDeposit);

// // Every Method :- If we want to test for the condition
// console.log(movements.every(mov => typeof mov === 'number')); // true
// console.log(movements.every(mov => typeof mov === 'string')); // false
// console.log(account4.movements.every(mov => mov > 0)); // true

// // Write seperate function for callback argument
// const deposit = mov => mov > 0;
// console.log(account4.movements.every(deposit)); // true

/* FLAT and FLATMAP METHODS */

// const arr = [[1, 2, 3], [4, 5, 6], 7, 8];

// // Wthout Flat method
// console.log(arr);
// // With Flat method
// console.log(arr.flat());

// const arrDeeperNested = [[[1, 2], 3], [4, [5, 6]], 7, 8];
// console.log(arrDeeperNested.flat()); // Wthout Depth argument
// console.log(arrDeeperNested.flat(2)); // Wth Depth argument

/* SORTING ARRAYS */

// // With Strings
// const owners = ['Ajay', 'Vijay', 'Komal', 'Sachin'];
// console.log(owners.sort());
// console.log(owners);

// // With Numbers
// const scores = [23, 111, -635, 32, -711, 13];
// console.log(scores.sort()); // This will not work as we want.

// If(Return < 0), then A will be before B
// If(Return > 0), then B will be before A

// // Ascending Order Sorting
// scores.sort((a, b) => {
//   if (a > b) {
//     return 1;
//   }
//   if (b > a) {
//     return -1;
//   }
// });
// console.log(scores);

// // Descending Order Sorting
// scores.sort((a, b) => {
//   if (a > b) {
//     return -1;
//   }
//   if (b > a) {
//     return 1;
//   }
// });
// console.log(scores);

/* MORE WAYS OF CREATING AND FILLING ARRAYS */

// console.log([1, 2, 3, 4, 5, 6, 7]); // This will create an array
// console.log(new Array(1, 2, 3, 4, 5, 6, 7)); // This will create an array
// console.log([3]); // This will create an array
// const x = new Array(7); // This will create an array, But this will create an empty array with length "7"
// console.log(x);
// x.map(() => 5);
// console.log(x);

// Empty Arrays + Fill Method
// x.fill(3);
// console.log(x);
// x.fill(3, 2);
// console.log(x);

// x.fill(1, 3, 5);
// console.log(x);

// const arr = [32, 45, 12, 65, 334, 323, 222];
// console.log(arr);
// arr.fill(23, 4, 6);
// console.log(arr);

// const y = Array.from({ length: 7 }, () => 8);
// console.log(y);

// const z = Array.from({ length: 9 }, (_curr, i) => i + 11);
// console.log(z);

// // Getting the web elements value in the array
// labelBalance.addEventListener('click', function () {
//   // Way 1
//   const movementsUI = Array.from(
//     document.querySelectorAll('.movements__value'),
//     el => Number(el.textContent.replace('€', ''))
//   );
//   console.log(movementsUI);

//   // Way 2
//   const movementUI2 = [...document.querySelectorAll('.movements__value')];
//   console.log(movementUI2.map(mov => Number(mov.textContent)));
// });
