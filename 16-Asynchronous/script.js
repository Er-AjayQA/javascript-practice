'use strict';

// https://countries-api-836d.onrender.com/countries/
// https://restcountries.com/v2/name/{country-name}

const btn = document.querySelector('.btn-country');
const countriesContainer = document.querySelector('.countries');

///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////
//////////// APPLICATION //////////////

/* Function for rendering the Country card html */
const renderCountry = function (data, className = '') {
  // Showing the data in the Frontend HTML format
  const html = `       <article class="country ${className}">
          <img class="country__img" src="${data.flag}" />
          <div class="country__data">
            <h3 class="country__name">${data.name}</h3>
            <h4 class="country__region">${data.region}</h4>
            <p class="country__row"><span>👫</span>${(
              +data.population / 1000000
            ).toFixed(1)}</p>
            <p class="country__row"><span>🗣️</span>${data.languages[0].name}</p>
            <p class="country__row"><span>💰</span>${
              data.currencies[0].name
            }</p>
          </div>
        </article>`;

  // countriesContainer.style.opacity = '1';
  countriesContainer.insertAdjacentHTML('beforeend', html);
};

/* Function for rendering the Error Message */
const renderError = function (msg) {
  countriesContainer.insertAdjacentText('beforeend', msg);
  // countriesContainer.style.opacity = 1;
};

/* Function for getting JSON from promises responses */
const getJSON = function (url, errorMsg) {
  return fetch(url).then(response => {
    if (!response.ok) {
      throw new Error(`${errorMsg} (${response.status})`);
    }

    return response.json();
  });
};

const getCountryData = function (country) {
  // Fetch Parent Country
  getJSON(`https://restcountries.com/v2/name/${country}`, 'Country not found')
    .then(data => {
      renderCountry(data[0]);
      const neighbour = data[0].borders?.[0];
      if (!neighbour) throw new Error(`Neighbour country not found`);

      // Fetch Neighbour Country
      return getJSON(
        `https://restcountries.com/v2/alpha/${neighbour}`,
        'Something Went Wrong'
      );
    })
    .then(data => renderCountry(data, 'neighbour'))
    // Handling the Promise Rejection by using "Catch" function
    .catch(err => {
      console.error(`${err}`);
      renderError(`Something Went Wrong ❌ ${err.message}. Try again!!`);
    })
    // Code in "Finally" function will called in both Fulfilled and Rejection of Promise
    .finally(() => {
      countriesContainer.style.opacity = 1;
    });
};

btn.addEventListener('click', function () {
  getCountryData('portugal');
  // getCountryData('waewea');
});

// getCountryData('australia');

///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////
////////// PRACTICE CLASSES ///////////

/* OLD WAY OF CALLING AJAX HTTP REQUESTS */

// const getCountryAndNeighbour = function (country) {
//   // AJAX call country 1
//   const request = new XMLHttpRequest();
//   request.open('GET', `https://restcountries.com/v2/name/${country}`);
//   request.send();

//   request.addEventListener('load', function () {
//     const [data] = JSON.parse(this.responseText); //  Need destructring because Countries name is array of countries names.
//     console.log(data);

//     // Render country 1
//     renderCountry(data);

//     // Get Neighbour Country
//     const neighbour = data.borders?.[0];
//     console.log(neighbour);

//     if (!neighbour) return;

//     // AJAX call country 2 => AJAX call 2 is b ased on AJAX call 1
//     const request2 = new XMLHttpRequest();
//     request2.open('GET', `https://restcountries.com/v2/alpha/${neighbour}`);
//     request2.send();

//     request2.addEventListener('load', function () {
//       const data2 = JSON.parse(this.responseText); // Here we don't need destructring because Country code is unique and result is also unique

//       // Render country 2
//       renderCountry(data2, 'neighbour');
//     });
//   });
// };
// getCountryAndNeighbour('usa');

// // CallBack Hell Example:-

// setTimeout(() => {
//   console.log('1 second passed');
//   setTimeout(() => {
//     console.log('2 second passed');
//     setTimeout(() => {
//       console.log('3 second passed');
//       setTimeout(() => {
//         console.log('4 second passed');
//       }, 1000);
//     }, 1000);
//   }, 1000);
// }, 1000);

/* CODE WITHOUT HELPER FUNCTION */

// const getCountryData = function (country) {
//   // Fetch Parent Country
//   const request = fetch(`https://restcountries.com/v2/name/${country}`)
//     .then(response => {
//       console.log(response);

//       if (!response.ok) {
//         throw new Error(`Country not found (${response.status})`);
//       }

//       return response.json();
//     })
//     .then(data => {
//       console.log(data[0]);
//       renderCountry(data[0]);
//       const neighbour = data[0].borders?.[0];
//       if (!neighbour) return;

//       // Fetch Neighbour Country
//       return fetch(`https://restcountries.com/v2/alpha/${neighbour}`);
//     })
//     .then(response => response.json())
//     .then(data => renderCountry(data, 'neighbour'))
//     .catch(err => {
//       console.error(`${err} ❌`);
//       renderError(`Something Went Wrong ❌ ${err.message}. Try again!!`);
//     }) // Adding the ERROR at End of the Chain.
//     .finally(() => {
//       countriesContainer.style.opacity = 1;
//     });
// };

/* THE EVENT LOOP IN PRACTICE */

// console.log('Test Start');

// setTimeout(() => console.log('0 sec timer'), 0);
// Promise.resolve('resolved promise 1').then(res => console.log(res));

// Promise.resolve('Resolved Promise 2').then(res => {
//   for (let i = 0; i <= 1000000000; i++) {}
//   console.log(res);
// });
// console.log('Test End');

/* BUILDING A PROMISE */

// // 1.) Building the Promise
// const lotteryPromise = new Promise(function (resolve, reject) {
//   console.log('Lottery draw is happening 🔮');
//   setTimeout(function () {
//     if (Math.random() >= 0.5) {
//       resolve('You Win!!');
//     } else {
//       reject(new Error('You Lost!!!'));
//     }
//   }, 2000);
// });

// // 1.) Consuming the promise just build above

// lotteryPromise.then(res => console.log(res)).catch(err => console.error(err));

// // Promisifying SetTimeout

// const wait = function (seconds) {
//   return new Promise(function (resolve) {
//     setTimeout(resolve, seconds * 1000);
//   });
// };

// Consuming the above Promise
// wait(2)
//   .then(() => {
//     console.log('I waited for 2 sec');
//     return wait(1);
//   })
//   .then(() => console.log('I waited for 1 second more'));

// CallBack-Based Function Example:-
// setTimeout(() => {
//   console.log('1 second passed');
//   setTimeout(() => {
//     console.log('2 second passed');
//     setTimeout(() => {
//       console.log('3 second passed');
//       setTimeout(() => {
//         console.log('4 second passed');
//       }, 1000);
//     }, 1000);
//   }, 1000);
// }, 1000);

// // Promise-Based Function Example:-
// wait(1)
//   .then(() => {
//     console.log('I waited for 1 sec');
//     return wait(2);
//   })
//   .then(() => {
//     console.log('I waited for 2 second more');
//     return wait(3);
//   })
//   .then(() => {
//     console.log('I waited for 3 second more');
//     return wait(4);
//   })
//   .then(() => {
//     console.log('I waited for 4 second more');
//   });

// Promise.resolve('abc').then(res => console.log(res));
// Promise.reject(new Error('Problem')).catch(err => console.error(err));

/* PROMISIFYING THE GEOLOCATION API */

// const gettingLocation = function () {
//   return new Promise(function (resolve, reject) {
//     // navigator.geolocation.getCurrentPosition(
//     //   position => resolve(position),
//     //   err => reject(new Error(err))
//     // );

//     // Same as above commented code
//     navigator.geolocation.getCurrentPosition(resolve, reject);
//   });
// };
// gettingLocation()
//   .then(res => console.log(res))
//   .catch(err => console.error(new Error(err)));

// console.log('Getting Position');

// const whereAmI = function () {
//   gettingLocation()
//     .then(pos => {
//       const { latitude: lat, longitude: lng } = pos.coords;
//       console.log(lat, lng);
//       return fetch(`https:geocode.xyz/${lat},${lng}?geoit=json`);
//     })
//     .then(res => {
//       if (!res.ok) throw new Error(`Problem with geolocation (${res.status})`);
//       return res.json();
//     })
//     .then(data => {
//       console.log(data);
//       console.log(`You are from ${data.country}`);
//       return fetch(`https://restcountries.com/v2/name/${data.country}`);
//     })
//     .then(err => {
//       if (!res.ok) throw new Error(`Country not found (${res.status})`);
//       return res.json();
//     })
//     .then(data => {
//       console.log(data);
//       renderCountry(data[0]);
//     })
//     .catch(err => console.error(`${err.message}`));
// };

// btn.addEventListener('click', whereAmI);

/* CONSUMING PROMISES WITH ASYNCH/AWAIT */

// const gettingLocation = function () {
//   return new Promise(function (resolve, reject) {
//     navigator.geolocation.getCurrentPosition(resolve, reject);
//   });
// };

// const whereAmI = async function () {
//   const pos = await gettingLocation();
//   const { latitude: lat, longitude: lng } = pos.coords;
//   console.log(lat, lng);
//   const resGeo = await fetch(`https://geocode.xyz/${lat},${lng}?geoit=json`);
//   console.log(resGeo);
//   const prsnData = await resGeo.json();
//   console.log(prsnData);

//   const res = await fetch(
//     `https://restcountries.com/v2/name/${prsnData.country}`
//   );
//   console.log(res);
//   const data = await res.json();
//   console.log(data);

//   renderCountry(data[1]);
//   countriesContainer.style.opacity = 1;
// };
// whereAmI();
// // console.log('First');

/* ERROR HANDLING WITH TRY CATCH */

// // Simpkle example of try...catch
// try {
//   let y = 2;
//   const x = 8;
//   console.log(`${y}, ${x}`);

//   // Error here
//   x = 10;
//   console.log(x);
// } catch (error) {
//   alert(error.message);
// }

// const gettingLocation = function () {
//   return new Promise(function (resolve, reject) {
//     navigator.geolocation.getCurrentPosition(resolve, reject);
//   });
// };

// const whereAmI = async function () {
//   try {
//     const pos = await gettingLocation();
//     const { latitude: lat, longitude: lng } = pos.coords;
//     console.log(lat, lng);
//     const resGeo = await fetch(`https://geocode.xyz/${lat},${lng}?geoit=json`);
//     console.log(resGeo);

//     // Throwing Error manually to test try catch block
//     if (!resGeo.ok) throw new Error(`Problem in getting location data.`);
//     const prsnData = await resGeo.json();
//     console.log(prsnData);

//     const res = await fetch(
//       `https://restcountries.com/v2/name/${prsnData.country}`
//     );
//     console.log(res);
//     // Throwing Error manually to test try catch block
//     if (!res.ok) throw new Error(`Problem in getting country data.`);
//     const data = await res.json();
//     console.log(data);

//     renderCountry(data[1]);
//     countriesContainer.style.opacity = 1;
//   } catch (err) {
//     console.error(err.message);
//     renderError(`${err.message}`);
//     countriesContainer.style.opacity = 1;
//   }
// };

// whereAmI();

// console.log('First');

/* RETURNING VALUES FROM ASYNC FUNCTIONS */

// const gettingLocation = function () {
//   return new Promise(function (resolve, reject) {
//     navigator.geolocation.getCurrentPosition(resolve, reject);
//   });
// };

// const whereAmI = async function () {
//   try {
//     const pos = await gettingLocation();
//     const { latitude: lat, longitude: lng } = pos.coords;
//     console.log(lat, lng);
//     const resGeo = await fetch(`https://geocode.xyz/${lat},${lng}?geoit=json`);
//     console.log(resGeo);

//     // Throwing Error manually to test try catch block
//     if (!resGeo.ok) throw new Error(`Problem in getting location data.`);
//     const prsnData = await resGeo.json();
//     console.log(prsnData);

//     const res = await fetch(
//       `https://restcountries.com/v2/name/${prsnData.country}`
//     );
//     console.log(res);
//     // Throwing Error manually to test try catch block
//     if (!res.ok) throw new Error(`Problem in getting country data.`);
//     const data = await res.json();
//     console.log(data);

//     renderCountry(data[1]);
//     countriesContainer.style.opacity = 1;

//     return `You are in ${prsnData.city}, ${prsnData.country}`;
//   } catch (err) {
//     console.error(err.message);
//     renderError(`${err.message}`);
//     countriesContainer.style.opacity = 1;

//     // Reject promise returned from promise => Rethrowing error so that it can be propagate below.
//     throw err;
//   }
// };

// // whereAmI();

// // const city = whereAmI();
// // console.log(city);

// // whereAmI()
// //   .then(city => console.log(city))
// //   .catch(err => console.error(err.message))
// //   .finally(() => console.log('Finally block displatyed'));

// (async function () {
//   try {
//     const city = await whereAmI();
//     console.log(city);
//   } catch (err) {
//     console.error(err.message);
//   }
//   console.log('Finally block displatyed');
// })();

/* RUNNING PROMISES PARALLELY */

// const get3Countries = async function (c1, c2, c3) {
//   try {
//     ////////
//     // Running all the 3 promises in sequence one by one.
//     // const [data1] = await getJSON(`https://restcountries.com/v2/name/${c1}`);
//     // const [data2] = await getJSON(`https://restcountries.com/v2/name/${c2}`);
//     // const [data3] = await getJSON(`https://restcountries.com/v2/name/${c3}`);
//     // console.log([data1.capital, data2.capital, data3.capital]);

//     ////////
//     // Running all the 3 promises in parallel.
//     const data = await Promise.all([
//       getJSON(`https://restcountries.com/v2/name/${c1}`),
//       getJSON(`https://restcountries.com/v2/name/${c2}`),
//       getJSON(`https://restcountries.com/v2/name/${c3}`),
//     ]);
//     data.map(d => console.log(d[0].capital));
//   } catch (err) {
//     console.error(err);
//   }
// };

// get3Countries('portugal', 'germany', 'canada');

/* OTHER PROMISE COMBINATTORS: RACE, ALLsETTLED, ANY */

// // 1. Promise.race()
// (async function () {
//   const res = await Promise.race([
//     getJSON(`https://restcountries.com/v2/name/${'portugal'}`),
//     getJSON(`https://restcountries.com/v2/name/${'germany'}`),
//     getJSON(`https://restcountries.com/v2/name/${'canada'}`),
//   ]);

//   console.log(res);
// })();

// const timeout = function (sec) {
//   return new Promise(function (_, reject) {
//     setTimeout(function () {
//       reject(new Error('Problem in getting details!!'));
//     }, sec * 1000);
//   });
// };

// Promise.race([
//   getJSON(`https://restcountries.com/v2/name/canada`),
//   timeout(0.5),
// ])
//   .then(res => console.log(res[0]))
//   .catch(err => console.log(err));

// // 2. Promise.allSettled()

// Promise.allSettled([
//   Promise.resolve('SUCCESS'),
//   Promise.reject('ERROR'),
//   Promise.resolve('ANOTHER SUCCESS'),
// ])
//   .then(res => console.log(res));

// // 3. Promise.any()

// Promise.any([
//   Promise.reject('ERROR'), // As this is rejected, so it will get ignored.
//   Promise.resolve('ANOTHER SUCCESS'),
//   Promise.resolve('SUCCESS'),
// ]).then(res => console.log(res));

////////////// END /////////////
