'use strict';

const btnScrollTo = document.querySelector('.btn--scroll-to');
const section1 = document.querySelector('#section--1');
const nav = document.querySelector('.nav');
const header = document.querySelector('.header');

///////////////////////////////////////
// MODAL WINDOW

const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');
const btnCloseModal = document.querySelector('.btn--close-modal');
const btnsOpenModal = document.querySelectorAll('.btn--show-modal');

const openModal = function () {
  modal.classList.remove('hidden');
  overlay.classList.remove('hidden');
};

const closeModal = function () {
  modal.classList.add('hidden');
  overlay.classList.add('hidden');
};

btnsOpenModal.forEach(btn => btn.addEventListener('click', openModal));

btnCloseModal.addEventListener('click', closeModal);
overlay.addEventListener('click', closeModal);

document.addEventListener('keydown', function (e) {
  if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
    closeModal();
  }
});

///////////////////////////////////////
// SCROLLING PAGE ON BUTTON CLICK

btnScrollTo.addEventListener('click', function (e) {
  // Modern Way Of Scrolling - Way-3
  section1.scrollIntoView({
    behavior: 'smooth',
  });
});

///////////////////////////////////////
// PAGE NAVIGATION

// // Without Event Delegation
// document.querySelectorAll('.nav__link').forEach(function (el) {
//   el.addEventListener('click', function (e) {
//     e.preventDefault();
//     const id = this.getAttribute('href');
//     console.log(id);
//     document.querySelector(id).scrollIntoView({
//       behavior: 'smooth',
//     });
//   });
// });

// With Event Delegation
// 1. Add event listener to the common parent element
// 2. Determine what element originated the event

document.querySelector('.nav__links').addEventListener('click', function (e) {
  e.preventDefault();
  const id = e.target.getAttribute('href');

  // Matching Strategy
  if (e.target.classList.contains('nav__link')) {
    document.querySelector(id).scrollIntoView({
      behavior: 'smooth',
    });
  }
});

///////////////////////////////////////
// TABS FUNCTIONALITY

const tabs = document.querySelectorAll('.operations__tab');
const tabsContainer = document.querySelector('.operations__tab-container');
const tabsContent = document.querySelectorAll('.operations__content');

tabsContainer.addEventListener('click', function (e) {
  e.preventDefault();
  const clicked = e.target.closest('.operations__tab');

  // Guard Clause
  if (!clicked) return;

  // Remove the Active Classess
  tabs.forEach(t => t.classList.remove('operations__tab--active'));
  tabsContent.forEach(c => c.classList.remove('operations__content--active'));

  // Activate the Tab
  clicked.classList.add('operations__tab--active');

  // Activate the Content Area
  document
    .querySelector(`.operations__content--${clicked.dataset.tab}`)
    .classList.add('operations__content--active');
});

///////////////////////////////////////
// MENU FADE ANIMATION

const handleHover = function (e) {
  if (e.target.classList.contains('nav__link')) {
    const link = e.target;
    const siblings = link.closest('.nav').querySelectorAll('.nav__link');
    const logo = link.closest('.nav').querySelector('img');

    siblings.forEach(el => {
      if (el !== link) el.style.opacity = this;
    });
    logo.style.opacity = this;
  }
};

// // Way-1 By Using Callback Function
// nav.addEventListener('mouseover', function (e) {
//   handleHover(e, 0.5);
// });

// nav.addEventListener('mouseout', function (e) {
//   handleHover(e, 1);
// });

// Way-2 Passing Argument Into Handler
nav.addEventListener('mouseover', handleHover.bind(0.5));

nav.addEventListener('mouseout', handleHover.bind(1));

///////////////////////////////////////
// STICKY HEADER

// // By Using Scroll Event => Altough it is not effiecient and not recommended to use
// const initialCords = section1.getBoundingClientRect();
// window.addEventListener('scroll', function () {
//   if (window.scrollY > initialCords.top) nav.classList.add('sticky');
//   else nav.classList.remove('sticky');
// });

// By Using Intersection Observer API

// Calculate the Height of Nav Dynamically
const navHeight = nav.getBoundingClientRect().height;

const stickyNav = function (entries) {
  const [entry] = entries;
  if (!entry.isIntersecting) nav.classList.add('sticky');
  else nav.classList.remove('sticky');
};
const headerObserver = new IntersectionObserver(stickyNav, {
  root: null,
  threshold: 0,
  rootMargin: `-${navHeight}px`,
});
headerObserver.observe(header);

///////////////////////////////////////
// REVEALING ELEMENTS ON SCROLL
const allSections = document.querySelectorAll('.section');
const revealSection = function (entries, observer) {
  const [entry] = entries;
  if (!entry.isIntersecting) return;
  entry.target.classList.remove('section--hidden');
  observer.unobserve(entry.target);
};

const sectionObserver = new IntersectionObserver(revealSection, {
  root: null,
  threshold: 0.15,
});

allSections.forEach(section => {
  sectionObserver.observe(section);
  // section.classList.add('section--hidden');
});

///////////////////////////////////////
// LAZY LOADING IMAGES

const imgTargets = document.querySelectorAll('img[data-src]');

const loadImg = function (entries, observer) {
  const [entry] = entries;
  if (!entry.isIntersecting) return;

  // Replace the Source Attribute
  entry.target.src = entry.target.dataset.src;
  entry.target.addEventListener('load', function (e) {
    entry.target.classList.remove('lazy-img');
  });

  observer.unobserve(entry.target);
};

const imgObserver = new IntersectionObserver(loadImg, {
  root: null,
  threshold: 0,
  rootMargin: '200px',
});

imgTargets.forEach(img => {
  imgObserver.observe(img);
});

///////////////////////////////////////
// SLIDER COMPONENT

const slider = function () {
  const slides = document.querySelectorAll('.slide');
  const slider = document.querySelector('.slider');
  const btnLeft = document.querySelector('.slider__btn--left');
  const btnRight = document.querySelector('.slider__btn--right');
  const dotContainer = document.querySelector('.dots');
  let currentSlide = 0;
  const maxSlide = slides.length - 1;

  // FUNCTIONS
  const createDots = function () {
    slides.forEach(function (_, i) {
      dotContainer.insertAdjacentHTML(
        'beforeend',
        `<button class="dots__dot" data-slide="${i}"></button>`
      );
    });
  };

  const activateDot = function (slide) {
    document
      .querySelectorAll('.dots__dot')
      .forEach(dot => dot.classList.remove('dots__dot--active'));

    document
      .querySelector(`.dots__dot[data-slide="${slide}"]`)
      .classList.add('dots__dot--active');
  };

  const goToSlide = function (slide) {
    slides.forEach((s, i) => {
      s.style.transform = `translateX(${100 * (i - slide)}%)`;
    });
  };

  // Go To Next Slide
  const nextSlide = function () {
    if (currentSlide === maxSlide) {
      currentSlide = 0;
    } else {
      currentSlide++;
    }

    goToSlide(currentSlide);
    // -100%, 0%, 100%, 200%
    activateDot(currentSlide);
  };

  // Go To Previous Slide
  const previousSlide = function () {
    if (currentSlide === 0) {
      currentSlide = maxSlide;
    } else {
      currentSlide--;
    }

    goToSlide(currentSlide);
    // -100%, 0%, 100%, 200%
    activateDot(currentSlide);
  };

  const init = function () {
    createDots();
    activateDot(0);
    goToSlide(0);
  };
  init();

  // EVENT HANDLERS
  btnRight.addEventListener('click', nextSlide);
  btnLeft.addEventListener('click', previousSlide);

  document.addEventListener('keydown', function (e) {
    console.log(e);
    if (e.key === 'ArrowRight') {
      nextSlide();
    }
    e.key === 'ArrowLeft' && previousSlide();
  });

  dotContainer.addEventListener('click', function (e) {
    if (e.target.classList.contains('dots__dot')) {
      const { slide } = e.target.dataset;
      goToSlide(slide);
      activateDot(slide);
    }
  });
};
slider();

///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////
// LECTURES

/* SELECTING, CREATING AND DELETING ELEMENTS */

// // Selecting Elements
// console.log(document.documentElement);
// console.log(document.head);
// console.log(document.body);

// const header = document.querySelector('.header');
// const allSection = document.querySelectorAll('.section');
// console.log(allSection);

// document.getElementById('section--1');
// const allButtons = document.getElementsByTagName('button');
// console.log(allButtons);

// const allBtn = document.getElementsByClassName('btn');
// console.log(allBtn);

// Creating and Inserting Elements
// // .innerAdjacentHTML
// const message = document.createElement('div');
// message.classList.add('cookie-message');
// message.textContent =
//   'We use cookies for improved functionalies and analytics.';
// message.innerHTML =
//   'We use cookies for improved functionalies and analytics. <button class="btn btn--close-cookies">Got it!</button>';

// Insert as First and last child
// header.prepend(message); // Add the node element as a first child element of applied parent element.
// header.append(message); // Add the node element as a last child element of applied parent element.
// header.append(message.cloneNode(true)); // Add the copy of the node element as a last child element of applied parent element.

// // Insert before and after as siblings
// header.before(message); // Add the node element as sibling before the specified element
// header.after(message); // Add the node element as sibling after the specified element

// // Delete Elements
// document
//   .querySelector('.btn--close-cookies')
//   .addEventListener('click', function () {
//     message.remove();
//   });

/* STYLES, ATTRIBUTE, AND CLASSES */

// // Styling
// message.style.backgroundColor = '#37383d'; // To set the inline css.
// message.style.width = '120%'; // To set the inline css.

// console.log(message.style.backgroundColor); // To get the inline css property if applied.
// console.log(getComputedStyle(message)); // To get all computes stylesheet css properties.
// console.log(getComputedStyle(message).color); // To get single stylesheet css property.
// console.log(getComputedStyle(message).height);

// message.style.height =
//   Number.parseFloat(getComputedStyle(message).height, 10) + 40 + 'px';
// const hgt = Number.parseFloat(getComputedStyle(message).height, 10);
// console.log(hgt);
// message.style.height = hgt + 40 + 'px';
// console.log(getComputedStyle(message).height);
// console.log(message.style.height);

// // Set the css property variables
// document.documentElement.style.setProperty('--color-primary', 'orangered');

// // ATTRIBUTES
// const logo = document.querySelector('.nav__logo');
// // Read standard attributes
// console.log(logo.src); // It gives the absolute URL.
// console.log(logo.getAttribute('src')); // It gives the Relative URL.
// console.log(logo.alt);

// // Read non-standard attribute values
// console.log(logo.getAttribute('designer'));

// // Set Attribute
// logo.alt = 'Ajay Logo'; // Way-1

// logo.setAttribute('name', 'headerLogo'); // Way-2
// console.log(logo.name);

// const link = document.querySelector('.btn--show-modal');
// console.log(link.href);
// console.log(link.getAttribute('href'));

// // Data Attributes
// console.log(logo.dataset); // Display the dataset object
// console.log(logo.dataset.versionNumber); // Access the property of "dataset" object

// // Classes
// logo.classList.add('class1', 'class2'); // Add classes to the specified element
// logo.classList.remove('class2', 'nav__logo'); // Remove classes from the specified element
// logo.classList.toggle('class2'); // Add class to the specified element if the class is not present or remove if class if already present
// console.log(logo.classList.contains('class1')); // Gives boolean value, check whether the given class is available on element or not

// // Don't use the below code for setting the class names.
// logo.className = 'AJay';

/* IMPLEMENT THE SMOOTH SCROLLING */

// const btnScrollTo = document.querySelector('.btn--scroll-to');
// const section1 = document.querySelector('#section--1');

// btnScrollTo.addEventListener('click', function (e) {
// const s1coords = section1.getBoundingClientRect();
// console.log(s1coords);

// console.log(e.target.getBoundingClientRect());
// console.log(window.pageXOffset, window.pageYOffset);
// console.log(document.documentElement.clientHeight);
// console.log(document.documentElement.clientWidth);

// // Scrolling Whithout smooth - Way-1
// window.scrollTo(
//   s1coords.left + window.pageXOffset,
//   s1coords.top + window.pageYOffset
// );

// // Smooth Animation - Way-2
// window.scrollTo({
//   left: s1coords.left + window.pageXOffset,
//   top: s1coords.top + window.pageYOffset,
//   behavior: 'smooth',
// });

//   // Modern way of scrolling - Way-3
//   section1.scrollIntoView({
//     behavior: 'smooth',
//   });
// });

// // Smooth scroll page to top by clicking on the footer logo
// const footLogo = document.querySelector('.footer__logo');

// footLogo.addEventListener('click', function () {
//   window.scrollTo({
//     left: 0,
//     top: 0,
//     behavior: 'smooth',
//   });
// });

/* TYPE OF EVENTS AND EVENTHANDLERS */

// const h1 = document.querySelector('h1');

// // Way-1:- To attach event handler.
// h1.addEventListener('mouseenter', function (e) {
//   alert('addEventListener: Great! You are reading the heading :D');
// });

// // Way-2:- To attach event handler.
// h1.onmouseenter = function (e) {
//   alert('addEventListener: Great! You are reading the heading :D');
// };

// // Code-1
// const alertH1 = function (e) {
//   alert('addEventListener: Great! You are reading the heading :D');
// };

// h1.addEventListener('mouseenter', alertH1);

// // Code-1.a
// const alertH1 = function (e) {
//   alert('addEventListener: Great! You are reading the heading :D');
//   h1.removeEventListener('mouseenter', alertH1);
// };

// h1.addEventListener('mouseenter', alertH1);

// // Way-3 <h1 onclick= "myFunc()"></h1>
// const myFunc = function () {
//   alert('Hello Alert!!');
// };

/* EVENT PROPAGATION: BUBBLING AND CAPTURING */

// // rgb(255,255,255)
// const randomInt = (min, max) =>
//   Math.floor(Math.random() * (max - min + 1) + min);
// const randomColor = () =>
//   `rgb(${randomInt(0, 255)},${randomInt(0, 255)},${randomInt(0, 255)})`;

// console.log(randomColor());

// document.querySelector('.nav__link').addEventListener('click', function (e) {
//   this.style.backgroundColor = randomColor();
//   console.log('LINK', e.target, e.currentTarget);

//   // // Stop Propagation
//   // e.stopPropagation();
// });

// document.querySelector('.nav__links').addEventListener('click', function (e) {
//   this.style.backgroundColor = randomColor();
//   console.log('CONTAINER', e.target, e.currentTarget);
//   // // Stop Propagation
//   // e.stopPropagation();
// });

// document.querySelector('.nav').addEventListener(
//   'click',
//   function (e) {
//     this.style.backgroundColor = randomColor();
//     console.log('NAV', e.target, e.currentTarget);
//   },
//   true
// );

/* EVENT DELEGATION: IMPLEMENTING PGAE NAVIGATION */

/* DOM TRAVERESING */

// const h1 = document.querySelector('h1');
// console.log(h1);

// // Going Downwards: Selecting child elements.
// console.log(h1.querySelectorAll('.highlight')); // Simple way.
// console.log(h1.childNodes); // To get the nodeList of all elements.
// console.log(h1.children); // To get the HTML collection of elements.

// h1.firstElementChild.style.color = '#fff'; // To get the first direct child element.
// h1.lastElementChild.style.color = '#000'; // To get the last direct child element.

// // Going Upwards: Selecting Parent elements.
// console.log(h1.parentNode); // To get the parent node.
// console.log(h1.parentElement); // To get the direct parent HTML element.
// h1.closest('.header').style.background = 'var(--gradient-secondary)'; // To get any parent element in DOM tree by query selector, class, ID, etc....
// console.log(h1.closest('h1'));

// // Going Upwards: Selecting Sibling Elements
// console.log(h1.previousElementSibling); // To get the previous sibling element.
// console.log(h1.previousSibling); // To get previous sibling node.
// console.log(h1.nextElementSibling); // To get the next sibling element.
// console.log(h1.nextSibling); // To get next sibling node.

// console.log(h1.parentElement.children);

// // Example Code
// [...h1.parentElement.children].forEach(function (el) {
//   if (el !== h1) {
//     el.style.transform = 'scale(0.5)';
//   }
// });

/* PASSING ARGUMENTS TO EVENTHANDLERS */

/* INTERSECTIONOBSERVER API */

// // By using scroll event => Altough it is not effiecient and not recommended to use
// const initialCords = section1.getBoundingClientRect();
// window.addEventListener('scroll', function () {
//   if (window.scrollY > initialCords.top) nav.classList.add('sticky');
//   else nav.classList.remove('sticky');
// });

// // By using Intersection Observer API
// const obsCallback = function (entries, observer) {
//   entries.forEach(entry => {
//     console.log(entry);
//   });
// };
// const obsOptions = {
//   root: null,
//   threshold: [0, 0.2],
// };
// const observer = new IntersectionObserver(obsCallback, obsOptions);
// observer.observe(section1);

/* REVEALING ELEMENTS ON SCROLL */

/* CROUSEL - IMAGE SLIDER */

// const slider = function () {
//   const slides = document.querySelectorAll('.slide');
//   const slider = document.querySelector('.slider');
//   const btnLeft = document.querySelector('.slider__btn--left');
//   const btnRight = document.querySelector('.slider__btn--right');
//   const dotContainer = document.querySelector('.dots');
//   let currentSlide = 0;
//   const maxSlide = slides.length - 1;

//   // FUNCTIONS
//   const createDots = function () {
//     slides.forEach(function (_, i) {
//       dotContainer.insertAdjacentHTML(
//         'beforeend',
//         `<button class="dots__dot" data-slide="${i}"></button>`
//       );
//     });
//   };

//   const activateDot = function (slide) {
//     document
//       .querySelectorAll('.dots__dot')
//       .forEach(dot => dot.classList.remove('dots__dot--active'));

//     document
//       .querySelector(`.dots__dot[data-slide="${slide}"]`)
//       .classList.add('dots__dot--active');
//   };

//   const goToSlide = function (slide) {
//     slides.forEach((s, i) => {
//       s.style.transform = `translateX(${100 * (i - slide)}%)`;
//     });
//   };

//   // Go To Next Slide
//   const nextSlide = function () {
//     if (currentSlide === maxSlide) {
//       currentSlide = 0;
//     } else {
//       currentSlide++;
//     }

//     goToSlide(currentSlide);
//     // -100%, 0%, 100%, 200%
//     activateDot(currentSlide);
//   };

//   // Go To Previous Slide
//   const previousSlide = function () {
//     if (currentSlide === 0) {
//       currentSlide = maxSlide;
//     } else {
//       currentSlide--;
//     }

//     goToSlide(currentSlide);
//     // -100%, 0%, 100%, 200%
//     activateDot(currentSlide);
//   };

//   const init = function () {
//     createDots();
//     activateDot(0);
//     goToSlide(0);
//   };
//   init();

//   // EVENT HANDLERS
//   btnRight.addEventListener('click', nextSlide);
//   btnLeft.addEventListener('click', previousSlide);

//   document.addEventListener('keydown', function (e) {
//     console.log(e);
//     if (e.key === 'ArrowRight') {
//       nextSlide();
//     }
//     e.key === 'ArrowLeft' && previousSlide();
//   });

//   dotContainer.addEventListener('click', function (e) {
//     if (e.target.classList.contains('dots__dot')) {
//       const { slide } = e.target.dataset;
//       goToSlide(slide);
//       activateDot(slide);
//     }
//   });
// };
// slider();

/* LIFECYCLE DOM EVENTS */

// // "DOMContentLoaded" Event
// document.addEventListener('DOMContentLoaded', function (e) {
//   console.log('HTML parsed and DOM tree build!!', e);
// });

// // "load" Event
// window.addEventListener('load', function (e) {
//   console.log('Page fully loaded!!', e);
// });

// // "beforeunload" Event
// window.addEventListener('beforeunload', function (e) {
//   e.preventDefault();
//   console.log(e);
//   e.returnValue = '';
// });

/* EFFICIENT SCRIPT LOADING: DEFER and ASYNC */
