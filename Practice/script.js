// ================= ADDING REMOVING ELEMENTS ================= //

// push();
// pop();
// shift();
// unshift();
// splice();
// slice();
// concat();
// forEach();
// map();
// reduce();
// reduceRight();
// flat();
// flatMap();
// filter();
// find();
// findIndex();
// findLast();
// findLastIndex();
// some();
// every();
// sort();
// includes();
// indexOf();
// lastIndexOf();
// reverse();
// copyWithin();
// fill();
// length(property);
// toString();
// toLocaleString();
// join();
// entries();
// keys();
// values();
// Array.isArray();
// Array.from();
// Array.of();

let products = [
  { name: "Laptop", categories: ["Electronics", "Computers"] },
  { name: "Shirt", categories: ["Apparel"] },
  { name: "Coffee Mug", categories: ["Kitchen", "DrinkWare"] },
];
let date = new Date();
const arr = [3, 33, 1, 12, 43, 22, 33];
const arr2 = [4, [3, 2, 3, 1], 12, 43, [20, 33]];

const data = Array.isArray(products);
console.log(data);
