'use strict';

// Dummy Code

// document.querySelector('.message').textContent = 'Correct Number!';
// console.log(document.querySelector('.message').textContent);
// document.querySelector('.number').textContent = 13;
// document.querySelector('.score').textContent = 20;
// document.querySelector('.guess').value = 50;
// console.log(document.querySelector('.guess').value);

// GAME LOGIC IMPLEMENTATION

let secreteNumber = Math.trunc(Math.random() * 20) + 1;
console.log(secreteNumber);
let score = 20;
let highscore = 0;

const displayMessage = message => {
  document.querySelector('.message').textContent = message;
};

const displayNumber = number => {
  document.querySelector('.number').textContent = number;
};

const setBodyColor = colorValue => {
  document.querySelector('body').style.backgroundColor = colorValue;
};

const setNumberWidth = value => {
  document.querySelector('.number').style.width = value;
};

// PLAY GAME
document.querySelector('.check').addEventListener('click', function () {
  const guess = Number(document.querySelector('.guess').value);

  // When there is no input.
  if (!guess) {
    displayMessage('⛔ No Number !!');

    // When player win the match.
  } else if (guess === secreteNumber) {
    setBodyColor('#60b347');
    setNumberWidth('30rem');
    displayNumber(secreteNumber);
    displayMessage('✔ Correct Answer !!');

    // Setting High Score.
    if (score > highscore) {
      highscore = score;
      document.querySelector('.highscore').textContent = highscore;
    }

    // When guess is wrong
  } else if (guess !== secreteNumber) {
    if (score > 1) {
      displayMessage(
        guess > secreteNumber ? '📈 Too High !!' : '📉 Too Low !!'
      );
      score--;
      document.querySelector('.score').textContent = score;
    } else {
      displayMessage('👎 You Lost the game !!');
      document.querySelector('.score').textContent = 0;
    }
  }
});

// RESET GAME
document.querySelector('.again').addEventListener('click', function () {
  score = 20;
  secreteNumber = Math.trunc(Math.random() * 20) + 1;
  displayMessage('Start guessing...');
  document.querySelector('.score').textContent = score;
  displayNumber('?');
  document.querySelector('.guess').value = '';
  setBodyColor('#222');
  setNumberWidth('15rem');
});
