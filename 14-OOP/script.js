'use strict';

/* CONSTRUCTOR FUNCTIONS AND THE NEW OPERATOR */

// // 1. A new {} empty object is created
// // 2. The function is called, and "this" keyword set/point to this newely created object. => this= {}
// // 3. This newely created object linked to a prototype.
// // 4. Function automatically return the empty object {}

// const Person = function (firstName, birthYear) {
//   // Instance properties
//   this.firstName = firstName;
//   this.birthYear = birthYear;

//   //   // Bad practice to create methods in constructors.
//   // 5. Never create the methods in the constructors, because when each time new object is created then this method also created each time.
//   //   this.calcAge = function () {
//   //     console.log(2024 - this.birthYear);
//   //   };
// };
// const ajay = new Person('Ajay', 1993);
// console.log(ajay);
// const vijay = new Person('Ajay', 1989);
// console.log(vijay);

/* PROTOTYPES */

// // Setting the methods on prototype.
// Person.prototype.calcAge = function () {
//   console.log(2024 - this.birthYear);
// };

// ajay.calcAge();
// vijay.calcAge();
// // Prototype of objects
// console.log(ajay.__proto__);
// console.log(vijay.__proto__);
// console.log(ajay.__proto__ === Person.prototype);
// console.log(Person.prototype.isPrototypeOf(ajay));
// console.log(Person.prototype.isPrototypeOf(Person));
// console.log(ajay.__proto__ === Person.__proto__);

// // Setting the properties on prototype
// Person.prototype.species = 'Homo Sapiens';
// console.log(ajay.species);
// console.log(vijay.species);
// console.log(ajay.hasOwnProperty('firstName'));
// console.log(ajay.hasOwnProperty('species'));

/* PROTOTYPAL INHERITANCE AND THE PROTOTYPE CHAIN */

/* PROTOTYPAL INHERITANCE ON BUILT-IN OBJECTS */
// console.log(ajay.__proto__);
// console.log(ajay.__proto__.__proto__);
// const arr = [2, 3, 4, 5, 6, 7, 5, 2, 6];
// console.log(arr.__proto__);

// // Creating the method in the "Array" constructor
// Array.prototype.unique = function () {
//   return [...new Set(this)];
// };
// console.log(arr.unique());

/* ES6 CLASSES */

// // Class Expression
// const PersonCl = class {};

// // Class Declaration
// class PersonCl {
//   constructor(firstName, birthYear) {
//     this.firstName = firstName;
//     this.birthYear = birthYear;
//   }
//   // This method will added to the .prototype property of class PersonCl
//   calcAge() {
//     console.log(2024 - this.birthYear);
//   }
// }

// const myInfo = new PersonCl('Ajay Kumar', 1993);
// console.log(myInfo);
// myInfo.calcAge();
// const yourInfo = new PersonCl('Rohit', 1996);
// console.log(yourInfo);
// yourInfo.calcAge();

// console.log(myInfo.__proto__ === PersonCl.prototype);
// console.log(myInfo.hasOwnProperty('calcAge()'));

// // Adding methods manually to the "prototype" property of the "PersonCl" class
// PersonCl.prototype.greet = function () {
//   console.log(`${this.firstName}, Welcome to my Coding Classes!!`);
// };

// myInfo.greet(); // Calling the methods of PersonCl protype property.
// yourInfo.greet(); // Calling the methods of PersonCl protype property.

/* SETTERS AND GETTERS */

// const account = {
//   owner: 'Jonas Schmedtmann',
//   movements: [200, 455.23, -306.5, 25000, -642.21, -133.9, 79.97, 1300],

//   // Getter Method
//   get latest() {
//     return this.movements.slice(-1).pop();
//   },

//   // Setter Method
//   set latest(mov) {
//     this.movements.push(mov);
//   },
// };
// console.log(account.latest); // Accessing the "latest" getter method
// account.latest = 900; // Setting the value by using "latest" setter method
// console.log(account.latest); // Accessing the "latest" getter method
// console.log(account.movements);

// // Getter and Setter methods in Classes
// class PersonCl {
//   constructor(fullName, birthYear) {
//     this.fullName = fullName;
//     this.birthYear = birthYear;
//   }

//   // Getter method inside the class
//   get age() {
//     return 2024 - this.birthYear;
//   }

//   // Setter method for Setting a property that already exist
//   set fullName(name) {
//     console.log(name);
//     if (name.includes(' ')) this._fullName = name;
//     else alert(`${name} is not a full name.`);
//   }

//   get fullName() {
//     return this._fullName;
//   }
// }

// const myInfo = new PersonCl('Ajay Kumar', 1993);
// console.log(myInfo.age); // Executing the getter method
// console.log(myInfo.fullName);
// console.log(myInfo._fullName);
// console.log(myInfo);

// const yourInfo = new PersonCl('Vijay', 1996);
// console.log(yourInfo.fullName);
// console.log(yourInfo);

/* STATIC METHODS */

// // For Constructor Functions
// const PersonCl = function (firstName, birthYear) {
//   this.firstName = firstName;
//   this.birthYear = birthYear;
// };

// PersonCl.greet = function () {
//   console.log(`Hey!! there`);
//   console.log(this);
// };
// const ajay = new PersonCl('Ajay Kumar', 1993);
// PersonCl.greet();

// // For Classes
// class Person2 {
//   constructor(firstName, birthYear) {
//     this.firstName = firstName;
//     this.birthYear = birthYear;
//   }

//   static hey() {
//     console.log(`Hey!! there`);
//     console.log(this);
//   }
// }

// const info2 = new Person2('Vijay', 1989);
// Person2.hey();
// info2.hey();

/* OBJECT.CREATE */

// // Object Literal
// const PersonProto = {
//   calcAge() {
//     console.log(2024 - this.birthYear);
//   },

//   // Simple method to set the properties of the objects.
//   init(name, birthYear) {
//     this.name = name;
//     this.birthYear = birthYear;
//   },
// };

// const rahulInfo = Object.create(PersonProto); // Linking the object to prototype object manually by passing it as argument in "object.create()".

// // Setting the properties to the "rahulInfo" object
// rahulInfo.name = 'Rahul Bora';
// rahulInfo.birthYear = 1993;

// console.log(rahulInfo);
// rahulInfo.calcAge();
// console.log(rahulInfo.__proto__);

// // "amitInfo" object
// const amitInfo = Object.create(PersonProto);
// amitInfo.init('Amit Kumar', 1994);
// console.log(amitInfo);

/* INHERITANCE BETWEEN "CLASSES": CONSTRUCTORS FUNCTIONS */

// // 1. Using Constructors Functions

// // Person Constructor Function
// const Person = function (firstName, birthYear) {
//   this.firstName = firstName;
//   this.birthYear = birthYear;
// };

// Person.prototype.calcAge = function () {
//   console.log(2024 - this.birthYear);
// };

// // Student Constructor Function
// const Student = function (firstName, birthYear, course) {
//   // Don't create duplicate code.
//   // this.firstName = firstName;
//   // this.birthYear = birthYear;

//   // Work same as above code
//   Person.call(this, firstName, birthYear);
//   this.course = course;
// };

// // Linking the Student prototype to the Person Prototype by using Object.create
// Student.prototype = Object.create(Person.prototype);

// Student.prototype.introduce = function () {
//   console.log(`My name is ${this.firstName} and I study ${this.course}`);
// };

// const mikeInfo = new Student('Mike', 1993, 'Computer Science'); // Creating the object of "Student" constructor function.
// mikeInfo.introduce(); // Calling the prototype function of "Student" constructor function.
// mikeInfo.calcAge();

// console.log(mikeInfo.__proto__);
// console.log(mikeInfo.__proto__.__proto__);

// // Pointing the student constructor to it's own.
// Student.prototype.constructor = Student;
// console.log(Student.prototype.constructor);

/* INHERITANCE BETWEEN "CLASSES": ES6 CLASSES */

// // Person Parent class
// class Person {
//   constructor(fullName, birthYear) {
//     this.fullName = fullName;
//     this.birthYear = birthYear;
//   }

//   calcAge() {
//     console.log(`My Age is ${2024 - this.birthYear}.`);
//   }

//   greet() {
//     console.log(`Hey ${this.fullName}`);
//   }
// }

// // Child Student class inherit the Person parent class.
// class Student extends Person {
//   constructor(fullName, birthYear, course) {
//     // Calling the Parent class custructor => Always call first.
//     super(fullName, birthYear);
//     this.course = course;
//   }

//   introduce = function () {
//     console.log(`I am ${this.fullName} and I study ${this.course}`);
//   };

//   // Override the parent class method
//   calcAge() {
//     super.calcAge(); //  By using the "Super" keyword we can still call the parent class
//     console.log(
//       `My Age is ${2024 - this.birthYear} but I feel like ${
//         2024 - this.birthYear + 10
//       } year old.`
//     );
//   }
// }

// // const std1 = new Student('Ajay Kumar', 1993);
// // console.log(std1);

// const std2 = new Student('Akash Kumar', 1989, 'Non-Medical');
// console.log(std2);
// std2.calcAge();
// std2.introduce();

/* INHERITANCE BETWEEN "CLASSES": OBJECT.CREATE() */

// const PersonProto = {
//   init(fullName, birthYear) {
//     this.fullName = fullName;
//     this.birthYear = birthYear;
//   },

//   // Parent Method
//   calcAge() {
//     console.log(2024 - this.birthYear);
//   },
// };

// // Linking the StudentProto to the PersonProto
// const StudentProto = Object.create(PersonProto); // This will create an empty prototype object of StudentProto.

// StudentProto.init = function (fullName, birthYear, course) {
//   PersonProto.init.call(this, fullName, birthYear); // Using the parent function in child with point "this" to the child.
//   this.course = course;
// };

// // Adding function in StudentProto prototype
// StudentProto.introduce = function () {
//   console.log(`I am ${this.fullName} and I study ${this.course}`);
// };
// const std2 = Object.create(StudentProto); // Create an Object "std2" with prototype linked to the "StudentProto"

// std2.init('Ajay Kumar', 1993, 'Medical Science');
// console.log(std2);
// std2.calcAge();
// std2.introduce();

/* ANOTHER CLASS EXAMPLE */

// class Account {
//   constructor(owner, currency, pin) {
//     this.owner = owner;
//     this.currency = currency;
//     this.pin = pin;
//     this.movements = []; // Defining an movements array.
//     this.locale = navigator.language; // Defining the locale language using Navigator API.

//     console.log(`Thanks for opening an account, ${owner}.`);
//   }

//   // Method for pushing the data to the movements array
//   // These methods are the public interface of our object
//   deposit(val) {
//     this.movements.push(val);
//   }

//   withDrawal(val) {
//     this.deposit(-val); // Calling method inside the method.
//   }

//   approveLoan(amount) {
//     return amount < 2000;
//   }

//   requestLoan(amount) {
//     if (this.approveLoan(amount)) {
//       this.deposit(amount);
//       console.log('Loan Approved ✔');
//     } else console.log(`Loan can't be Approved ❌`);
//   }
// }

// const acc1 = new Account('jonas', 'EUR', 1111);
// console.log(acc1);

// // Adding the amounts to the movements array
// acc1.deposit(230);
// acc1.deposit(300);
// acc1.withDrawal(211);
// acc1.deposit(500);
// acc1.withDrawal(180);
// acc1.requestLoan(1300);
// acc1.approveLoan(5000); // We are able to access the internal methods of class

// // We can access the PIN from outside the Account class
// console.log(acc1.pin);

// // Note:- For the data privacy, we really need to use the Encapsulation.

/* ENCAPSULATION: PROTECTED PROPERTIES AND METHODS */

// class Account {
//   constructor(owner, currency, pin) {
//     this.owner = owner;
//     this.currency = currency;
//     this._pin = pin;
//     // Protected property
//     this._movements = [];
//     this.locale = navigator.language;

//     console.log(`Thanks for opening an account, ${owner}.`);
//   }

//   // Internal method of class
//   _approveLoan(amount) {
//     return amount < 2000;
//   }

//   // Public Interface Methods
//   deposit(val) {
//     this._movements.push(val);
//   }

//   withDrawal(val) {
//     this.deposit(-val);
//   }

//   requestLoan(amount) {
//     if (this._approveLoan(amount)) {
//       this.deposit(amount);
//       console.log('Loan Approved ✔');
//     } else console.log(`Loan can't be Approved ❌`);
//   }

//   getMovements() {
//     return this._movements;
//   }
// }

// const acc1 = new Account('jonas', 'EUR', 1111);
// console.log(acc1);
// acc1._movements.push(3000); // Can access the protected propeties from outside of the class
// acc1.deposit(230);
// acc1.withDrawal(211);
// acc1.requestLoan(1300);

// console.log(acc1._approveLoan(5000)); // Able to access the Internal method of class

// // Can access the movements by method
// console.log(acc1.getMovements());

/* ENCAPSULATION: PRIVATE PROPERTIES AND METHODS */

// class Account {
//   // 1.) Public Fields (Instances)
//   locale = navigator.language;

//   // 2.) Private Fields
//   #movements = [];
//   #pin;

//   constructor(owner, currency, pin) {
//     this.owner = owner;
//     this.currency = currency;
//     this.#pin = pin;
//     console.log(`Thanks for opening an account, ${owner}.`);
//   }

//   // 3.) Public Methods
//   deposit(val) {
//     this.#movements.push(val);
//   }

//   withDrawal(val) {
//     this.deposit(-val);
//   }

//   requestLoan(amount) {
//     if (this.#approveLoan(amount)) {
//       this.deposit(amount);
//       console.log('Loan Approved ✔');
//     } else console.log(`Loan can't be Approved ❌`);
//   }

//   getMovements() {
//     return this.#movements;
//   }

//   // 4.) Private Methods
//   #approveLoan(amount) {
//     return amount < 2000;
//   }

//   // 5.) Static Instances
//   static helper() {
//     console.log('I am the Static Helper function.');
//   }
// }

// const acc1 = new Account('jonas', 'EUR', 1111);
// console.log(acc1);
// acc1.deposit(230);
// acc1.withDrawal(211);
// acc1.requestLoan(1300);

// // Accessing the Private Fields
// // console.log(acc1.#movements.push()); // This gives an error Private fields can't be accessible.
// // console.log(acc1.#pin); // This gives an error Private fields can't be accessible.

// // Accessing the Private Fields
// // console.log(acc1.#approveLoan(2000)); // This gives an error Private fields can't be accessible.
// console.log(acc1.getMovements()); // This is accessible because it is Public Method

// // Accessing the Static Methods
// Account.helper();

/* CHAINING METHODS */

// class Account {
//   locale = navigator.language;
//   #movements = [];
//   #pin;

//   constructor(owner, currency, pin) {
//     this.owner = owner;
//     this.currency = currency;
//     this.#pin = pin;
//     console.log(`Thanks for opening an account, ${owner}.`);
//   }

//   deposit(val) {
//     this.#movements.push(val);
//     return this;
//   }

//   withDrawal(val) {
//     this.deposit(-val);
//     return this;
//   }

//   requestLoan(amount) {
//     if (this.#approveLoan(amount)) {
//       this.deposit(amount);
//       console.log('Loan Approved ✔');
//       return this;
//     } else {
//       console.log(`Loan can't be Approved ❌`);
//       return this;
//     }
//   }

//   getMovements() {
//     return this.#movements;
//   }

//   #approveLoan(amount) {
//     return amount <= 2000;
//   }

//   static helper() {
//     console.log('I am the Static Helper function.');
//   }
// }

// const acc1 = new Account('jonas', 'EUR', 1111);

// // Chaining Methods
// acc1
//   .deposit(300)
//   .deposit(1000)
//   .withDrawal(500)
//   .requestLoan(2000)
//   .withDrawal(4000);
// console.log(acc1.getMovements());

/* ES6 CLASSES SUMMARY   */
