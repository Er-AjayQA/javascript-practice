'use strict';

// Buttons
const btnsShowModal = document.querySelectorAll('.show-modal');
const btnCloseModal = document.querySelector('.close-modal');
// Elements
const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');

// Show Modal function
const showModal = function () {
  modal.classList.remove('hidden');
  overlay.classList.remove('hidden');
};

// Close Modal function
const closeModal = function () {
  modal.classList.add('hidden');
  overlay.classList.add('hidden');
};

// Open Modal Event
for (let i = 0; i < btnsShowModal.length; i++) {
  btnsShowModal[i].addEventListener('click', showModal);
}

// Close Modal Event
btnCloseModal.addEventListener('click', closeModal);
overlay.addEventListener('click', closeModal);

// Closing Modal when "Escape" key is pressed.
document.addEventListener('keydown', function (e) {
  if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
    closeModal();
  }
});
