'use strict';

// SCOPING
/*function calcAge(birthYear) {
  const age = 2023 - birthYear;

  function printAge() {
    const output = `${firstName}, You are ${age}, born in ${birthYear}`;
    console.log(output);

    if (birthYear >= 1981 && birthYear <= 1996) {
      var millenial = true;
      const str = `Oh!, and you are a millenial, ${firstName}`;
      console.log(str);

      function add(a, b) {
        return a + b;
      }
      console.log(add(2, 3));
    }
  }
  printAge();
  return age;
}

const firstName = 'Ajay';
calcAge(1993);*/

// (2) HOISTING

/* // Variables
console.log(me); // Output will be undefine
console.log(job); // Can't access before initialization
console.log(birthYear); // Can't access before initialization

var me = 'Ajay';
let job = 'Tester';
const birthYear = 1993;

// Functions

console.log(add(2, 3)); // Gives an output.
console.log(addExpr(5, 10)); // can't access before the initialization.
console.log(addArrow(4, 3)); // Can't access before the initialization.
console.log(subArrow(4, 3)); // Gives an error of "subArrow" is not defined. Because subArrow is undefined.

function add(a, b) {
  return a + b;
}

let addExpr = function (a, b) {
  return a + b;
};

const addArrow = (a, b) => a + b;

var subArrow = (a, b) => a - b;*/

// (3) Example of Hoisting

/*console.log(numProduct);

if (!numProduct) {
  deleteShoopingCart();
}
var numProduct = 10;
function deleteShoopingCart() {
  console.log('All products deleted!');
}

var x = 1;
let y = 2;
const z = 3;

console.log(x === window.x);
console.log(y === window.y);
console.log(z === window.z);
*/

// THIS KEYWORD

// Normal function
/*console.log(this); // Points to window object.
const calcAge = function (birthYear) {
  console.log(2023 - birthYear);
  console.log(this); // Undefined in "Strict mode", but points to window/global object in unstrict mode.
};
calcAge(1991);*/

// With arrow functions

/*var x = 100; // It is a window object
console.log(this.x); // Points to the window object
const calcAgeArrow = birthYear => {
  console.log(2023 - birthYear);
  console.log(this.x); // Points to the window object, that is parents object.
};
calcAgeArrow(1989);*/

// Methods of parent object

/*const myInfo = {
  birthYear: 1993,
  calcAge: function () {
    console.log(this); // Here "this" keyword points to the myInfo object
    console.log(2023 - this.birthYear);
  },
};
myInfo.calcAge();

// Depending upon who is calling the method
const matilda = {
  birthYear: 2017,
};
matilda.calcAge = myInfo.calcAge;
matilda.calcAge();

const f = myInfo.calcAge;
f();*/

// REGULAR FUNCTION VS. ARROW FUNCTION

// Example 1
/*var firstName = 'Vijay';

const myInfo = {
  firstName: 'Ajay',
  birthYear: 1993,
  calcAge: function () {
    console.log(`Hey! ${this.firstName}`);
    console.log(2023 - this.birthYear);
  },
  greet: () => {
    console.log(`Hey! ${this.firstName}`); // Points to the global this object, as arrow functions don't hve their own 'this' keyword.
  },
};
myInfo.calcAge();
myInfo.greet();*/

// Example 2
/* VERSION-1:- In the regular function call the "this" keyword is undefined.*/
// const myInfo = {
//   firstName: 'Ajay',
//   birthYear: 1993,
//   calcAge: function () {
//     console.log(2023 - this.birthYear);

//     const isMellenial = function () {
//       console.log(self);
//       console.log(self.birthYear >= 1981 && self.birthYear <= 1996);
//     };
//     isMellenial(); // Here this keyword will be undefined because it is a regular function call.
//   },
//   greet: () => {
//     console.log(`Hey! ${this.firstName}`);
//   },
// };

// myInfo.calcAge();

/* VERSION-2:- Solution of the regular function call by making it "Self=this*/
// const info = {
//   firstName: 'Ajay',
//   birthYear: 1993,
//   calcAge: function () {
//     console.log(2023 - this.birthYear);
//     const self = this;
//     const isMellenial = function () {
//       console.log(self);
//       console.log(self.birthYear >= 1981 && self.birthYear <= 1996);
//     };
//     isMellenial();
//   },
//   greet: () => {
//     console.log(`Hey! ${this.firstName}`);
//   },
// };
// info.calcAge();

/* VERSION-2:- Solution of the above problem in ES6*/
// const info = {
//   firstName: 'Ajay',
//   birthYear: 1993,
//   calcAge: function () {
//     console.log(2023 - this.birthYear);

//     const isMellenial = () => {
//       console.log(this);
//       console.log(this.birthYear >= 1981 && this.birthYear <= 1996);
//     };
//     isMellenial();
//   },
//   greet: () => {
//     console.log(`Hey! ${this.firstName}`);
//   },
// };
// info.calcAge();

/* ARGUMENTS KEYWORD */

// Example 1 :- In regular functions the arguments can acccept any numbers of parameters, even if they are not defined.
// const addExpr = function (a, b) {
//   console.log(arguments);
//   for (let i = 0; i <= arguments.length - 1; i++) {
//     console.log(arguments[i]);
//   }
//   return a + b;
// };

// addExpr(2, 3, 12, 23);

// // Example 2 :- In arrow functions the arguments is not defined. i.e arguments keyword only exist inf regular functions.
// var addArrow = (a, b) => {
//   console.log(arguments);

//   console.log(a + b);
// };
// addArrow(2, 3, 12, 34);

//PRIMITIVE Vs. OBJECTS

// Example 1:- Simple variables
/*let age = 30;
let oldAge = age;
age = 31;
// let oldAge = age;
console.log(age);
console.log(oldAge);

// Example 2:- Objects
const me = {
  name: 'Ajay',
  age: 35,
  family: ['Mom', 'Dad', 'brother', 'Sister'],
};
// This will not work 
// const friend = me;
// friend.age = 33;

// This will work 'Shallow Copy' 
const friend = Object.assign({}, me);
friend.age = 33;
friend.family.push('Cousin Brother', 'Cousin Sister');

console.log('Me :-', me);
console.log('Friend :-', friend);*/
