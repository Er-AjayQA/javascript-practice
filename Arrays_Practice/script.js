// ================= ADDING REMOVING ELEMENTS ================= //

// push();
// pop();
// shift();
// unshift();
// splice();
// concat();
// slice();

// ==== slice() ==== //
// const name = "Ajay Kumar";
// let arr = ["a", "b", "c", "d", "e"];
// console.log(name.slice(2, -3));
// console.log(arr.slice(2, 4));
// console.log(arr);

// ==== concat() ==== //
// let arr1 = ["a", "b", "c", "d", "e"];
// let arr2 = ["f", "g", "h", "i", "j"];
// const word1 = "Hello";
// const word2 = "World";
// console.log(arr1.concat(arr2));
// console.log(word1.concat(word2));
// console.log(word1.concat(" ").concat(word2));

// ==== splice() ==== //
// let arr = ["a", "b", "c", "d", "e"];
// console.log(arr.splice(2, 2));
// console.log(arr);

// ==== push() ==== //
// let arr = ["a", "b", "c", "d", "e"];
// console.log(arr.push("f"));
// console.log(arr);

// ==== unshift() ==== //
// let arr = ["a", "b", "c", "d", "e"];
// console.log(arr.unshift("f"));
// console.log(arr);

// ==== pop() ==== //
// let arr = ["a", "b", "c", "d", "e"];
// console.log(arr.pop());
// console.log(arr);

// ==== shift() ==== //
// let arr = ["a", "b", "c", "d", "e"];
// console.log(arr.shift());
// console.log(arr);

// ================= ITERATING AND TRANSFORMING ================= //

// forEach();
// map();
// reduce();
// reduceRight();
// flat();
// flatMap();

// ==== forEach() ==== //
// let arr = [6, 12, 3, 22, 2, 8];
// console.log(`Original Array => ${arr}`);
// arr.forEach((value, index, data) => {
//   console.log(value * 2);
//   console.log(`Indexes => ${index}`);
//   console.log(`Data => ${data}`);
// });

// ==== map() ==== //
// let arr = [6, 12, 3, 22, 2, 8];
// const arr2 = arr.map((value, index, array) => {
//   return value * index;
// });
// console.log(arr);
// console.log(arr2);

// let users = [
//   { firstName: "John", lastName: "Doe" },
//   { firstName: "Jane", lastName: "Smith" },
//   { firstName: "Emily", lastName: "Jones" },
// ];

// users.map((user) => {
//   console.log(user.firstName.concat(` ${user.lastName}`));
// });

// ==== reduce() ==== //
// let arr = [6, 12, 3, 22, 2, 8];
// const data = arr.reduce(
//   (Accumulator, value, index, array) => Accumulator + value,
//   0
// );
// console.log(data);

// ==== reduceRight() ==== //
// let arr = [6, 12, 3, 22, 2, 8];
// arr.reduceRight((acc, val) => {
//   console.log(val);
// });

// ==== flat() ==== //
// let arr = [6, [12, 3], 22, [2, 8], [32, 12], [44, 32]];
// const data = arr.flat();
// console.log(data);

// ==== flatMap() ==== //
// let products = [
//   { name: "Laptop", categories: ["Electronics", "Computers"] },
//   { name: "Shirt", categories: ["Apparel"] },
//   { name: "Coffee Mug", categories: ["Kitchen", "Drinkware"] },
// ];
// const data = products.flatMap((val) => {
//   return val.categories;
// });
// console.log(data);

// ================= SEARCHING AND FILTERING ================= //

// filter();
// find();
// findIndex();
// findLast();
// findLastIndex();
// includes();
// indexOf();
// lastIndexOf();
// some();
// every();

// ==== filter() ==== //
// let arr = [6, 12, 3, 22, 2, 8];
// const allEven = arr.filter((num) => {
//   return num % 2 === 0;
// });
// console.log(allEven);

// ==== find() ==== //
// let arr = [6, 12, 3, 22, 2, 8];
// const data = arr.find((num) => {
//   return num % 2 === 0;
// });
// console.log(data);

// ==== findIndex() ==== //
// let arr = [6, 12, 3, 22, 2, 8];
// const indexes = arr.findIndex((num) => num > 10);
// console.log(indexes);

// ==== includes() ==== //
// let arr = [6, 12, 3, 22, 2, 8];
// console.log(arr.includes(2));

// ==== indexof() ==== //
// let arr = [6, 12, 3, 22, 2, 8];
// console.log(arr.indexOf(2));

// ==== lastIndexOf() ==== //
// let arr = [6, 2, 12, 3, 22, 2, 8];
// console.log(arr.lastIndexOf(2));

// ==== some() ==== //
// let arr = [6, 12, 22, 2, 8, 6, 12, 3];
// const hasOdd = arr.some((val, index) => {
//   return val % 2 !== 0;
// });
// console.log(hasOdd);

// ==== every() ==== //
// let arr = [2, 4, 6, 8, 10, 11];
// const data = arr.every((val) => {
//   return val % 2 === 0;
// });
// console.log(data);

// ================= SORTING AND REORDERING ================= //

// sort();
// reverse();
// copyWithin();
// fill();

// ==== sort() ==== //
// let arr1 = [6, 12, 3, 22, 2, 8];
// let arr2 = [6, 12, 3, 22, 2, 8];
// // Ascending
// arr1.sort((a, b) => {
//   return a - b;
// });
// // Descending
// arr2.sort((a, b) => {
//   return b - a;
// });
// console.log(arr1);
// console.log(arr2);

// ==== reverse() ==== //
// let arr = [6, 12, 3, 22, 2, 8];
// console.log(arr.reverse());

// ==== copyWithin() ==== //
// let arr = [6, 12, 3, 22, 2, 8];
// console.log(arr.copyWithin(2, 3, 5));

// ==== fill() ==== //
// let arr = [6, 12, 3, 22, 4, 8];
// console.log(arr.fill(2, 2, 4));

// ================= ARRAY INSPECTION ================= //

// length(property);
// toString();
// toLocaleString();
// join();
// entries();
// keys();
// values();

// ==== length() ==== //
// let arr = [6, 12, 3, 22, 4, 7, 8];
// console.log(arr.length);

// ==== toString() ==== //
// let arr = [6, 12, 3, 22, 4, 7, 8];
// console.log(arr.toString());

// ==== toLocaleString() ==== //
// let arr = [6, 12, 3, 22, 4, 7, 8];
// let date = new Date();
// console.log(date.toLocaleString());

// ==== join() ==== //
// let arr = [6, 12, 3, 22, 4, 7, 8];
// const name = ["Ajay", "Kumar"];
// console.log(arr.join(" "));
// console.log(name.join("-"));

// ==== entries() ==== //
// let arr = [6, 12, 3, 22, 4, 7, 8];
// const iter = arr.entries();
// for (let val of iter) {
//   console.log(val);
// }

// ==== keys() ==== //
// let arr = [6, 12, 3, 22, 4, 7, 8];
// const keys = arr.keys();
// for (let i of keys) {
//   console.log(i);
// }

// ==== values() ==== //
// let arr = [6, 12, 3, 22, 4, 7, 8];
// const values = arr.values();
// for (let i of values) {
//   console.log(i);
// }

// ================= STATIC METHODS ================= //

// Array.isArray();
// Array.from();
// Array.of();

// ==== Array.isArray() ==== //
// let arr = [6, 12, 3, 22, 4, 7, 8];
// console.log(Array.isArray(arr));

// ==== Array.from() ==== //
// const name = "Ajay Kumar";
// console.log(Array.from(name));

// ==== Array.of() ==== //
// const name = "Ajay Kumar";
// console.log(Array.of("2,3,4,5"));
