'use strict';

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

/////////////////////////////////////////////////
// Data

// DIFFERENT DATA! Contains movement dates, currency and locale

const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 455.23, -306.5, 25000, -642.21, -133.9, 79.97, 1300],
  interestRate: 1.2, // %
  pin: 1111,

  movementsDates: [
    '2024-01-01T21:31:17.178Z',
    '2024-01-02T07:42:02.383Z',
    '2024-01-04T09:15:04.904Z',
    '2024-01-01T10:17:24.185Z',
    '2024-01-08T14:11:59.604Z',
    '2024-01-03T17:01:17.194Z',
    '2024-01-05T23:36:17.929Z',
    '2024-01-08T10:51:36.790Z',
  ],
  currency: 'EUR',
  locale: 'en-IN', // de-DE
};

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,

  movementsDates: [
    '2024-01-01T13:15:33.035Z',
    '2024-01-30T09:48:16.867Z',
    '2024-01-25T06:04:23.907Z',
    '2024-01-25T14:18:46.235Z',
    '2024-01-05T16:33:06.386Z',
    '2024-01-10T14:43:26.374Z',
    '2024-01-25T18:49:59.371Z',
    '2024-01-26T12:01:20.894Z',
  ],
  currency: 'USD',
  locale: 'en-US',
};

const accounts = [account1, account2];

/////////////////////////////////////////////////
// Elements
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const labelSumInterest = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');

/////////////////////////////////////////////////
// Functions

const formatMovementDates = function (date, locale) {
  const calcDaysPassed = (date1, date2) =>
    Math.round(Math.abs(date2 - date1) / (1000 * 60 * 60 * 24));

  const daysPassed = calcDaysPassed(new Date(), date);
  console.log(daysPassed);

  if (daysPassed === 0) return 'Today';
  if (daysPassed === 1) return 'Yesterday';
  if (daysPassed <= 7) return `${daysPassed} days ago`;
  else {
    // const day = `${date.getDate()}`.padStart(2, '0');
    // const month = `${date.getMonth() + 1}`.padStart(2, '0');
    // const year = date.getFullYear();
    // return `${day}/${month}/${year}`;

    return new Intl.DateTimeFormat(locale).format(date);
  }
};

const formatCurrency = function (value, locale, currency) {
  return new Intl.NumberFormat(locale, {
    style: 'currency',
    currency: currency,
  }).format(value);
};

const startlogouttimer = function () {
  const tick = function () {
    const min = String(Math.trunc(time / 60)).padStart(2, '0');
    const sec = time % 60;
    // In each call, print the remaining time to user.
    labelTimer.textContent = `${min}:${sec}`;

    // When 0 seconds remaining, stop timer and logout user.
    if (time === 0) {
      clearInterval(timer);
      // Display UI and message
      labelWelcome.textContent = 'Login to get started';
      containerApp.style.opacity = 0;
    }
    // Decrease timer
    time--;
  };

  // Setting time to 5 minutes
  let time = 600;
  // Call the timer every second
  tick();
  const timer = setInterval(tick, 1000);
  return timer;
};

const displayMovements = function (acc, sort = false) {
  containerMovements.innerHTML = '';

  const movs = sort
    ? acc.movements.slice().sort((a, b) => a - b)
    : acc.movements;

  movs.forEach(function (mov, i) {
    const type = mov > 0 ? 'deposit' : 'withdrawal';
    const date = new Date(acc.movementsDates[i]);
    const displayDate = formatMovementDates(date, acc.locale);
    const formattedMov = formatCurrency(mov, acc.locale, acc.currency);

    const html = `
      <div class="movements__row">
        <div class="movements__type movements__type--${type}">${
      i + 1
    } ${type}</div>
    <div class="movements__date">${displayDate}</div>
        <div class="movements__value">${formattedMov}</div>
      </div>
    `;

    containerMovements.insertAdjacentHTML('afterbegin', html);
  });
};

const calcDisplayBalance = function (acc) {
  acc.balance = acc.movements.reduce((acc, mov) => acc + mov, 0);
  labelBalance.textContent = `${formatCurrency(
    acc.balance,
    acc.locale,
    acc.currency
  )}`;
};

const calcDisplaySummary = function (acc) {
  const incomes = acc.movements
    .filter(mov => mov > 0)
    .reduce((acc, mov) => acc + mov, 0);
  labelSumIn.textContent = formatCurrency(incomes, acc.locale, acc.currency);

  const out = acc.movements
    .filter(mov => mov < 0)
    .reduce((acc, mov) => acc + mov, 0);
  labelSumOut.textContent = formatCurrency(out, acc.locale, acc.currency);

  const interest = acc.movements
    .filter(mov => mov > 0)
    .map(deposit => (deposit * acc.interestRate) / 100)
    .filter((int, i, arr) => {
      // console.log(arr);
      return int >= 1;
    })
    .reduce((acc, int) => acc + int, 0);
  labelSumInterest.textContent = formatCurrency(
    interest,
    acc.locale,
    acc.currency
  );
};

const createUsernames = function (accs) {
  accs.forEach(function (acc) {
    acc.username = acc.owner
      .toLowerCase()
      .split(' ')
      .map(name => name[0])
      .join('');
  });
};
createUsernames(accounts);

const updateUI = function (acc) {
  // Display movements
  displayMovements(acc);

  // Display balance
  calcDisplayBalance(acc);

  // Display summary
  calcDisplaySummary(acc);
};

///////////////////////////////////////
// Event handlers

// // Fake Always Logged In
let currentAccount, timer;
// currentAccount = account1;
// updateUI(currentAccount);
// containerApp.style.opacity = '1';

// // Experimenting with API
// const now = new Date();

// const options = {
//   hour: 'numeric',
//   minute: 'numeric',
//   day: '2-digit',
//   month: 'long',
//   // month: 'short',
//   year: 'numeric',
//   // weekday: 'long',
//   weekday: 'short',
//   // weekday: 'narrow',
// };

// const locale = navigator.language;

// labelDate.textContent = new Intl.DateTimeFormat('locale', options).format(now);

// Login
btnLogin.addEventListener('click', function (e) {
  // Prevent form from submitting
  e.preventDefault();

  currentAccount = accounts.find(
    acc => acc.username === inputLoginUsername.value
  );
  console.log(currentAccount);

  if (currentAccount?.pin === +inputLoginPin.value) {
    // Display UI and message
    labelWelcome.textContent = `Welcome back, ${
      currentAccount.owner.split(' ')[0]
    }`;
    containerApp.style.opacity = 100;

    // Create Current Date and time
    const now = new Date();
    // const day = `${now.getDate()}`.padStart(2, '0');
    // const month = `${now.getMonth() + 1}`.padStart(2, '0');
    // const year = now.getFullYear();
    // const hour = now.getHours();
    // const minute = `${now.getMinutes()}`.padStart(2, '0');
    // labelDate.textContent = `${day}/${month}/${year}, ${hour}:${minute}`;

    const options = {
      hour: 'numeric',
      minute: 'numeric',
      day: '2-digit',
      month: 'numeric',
      year: 'numeric',
    };

    const locale = currentAccount.locale;

    labelDate.textContent = `${new Intl.DateTimeFormat(locale, options).format(
      now
    )}`;

    // Clear input fields
    inputLoginUsername.value = inputLoginPin.value = '';
    inputLoginPin.blur();

    // Timer
    if (timer) clearInterval(timer);
    timer = startlogouttimer();
    // Update UI
    updateUI(currentAccount);
  }
});

btnTransfer.addEventListener('click', function (e) {
  e.preventDefault();
  const amount = +inputTransferAmount.value;
  const receiverAcc = accounts.find(
    acc => acc.username === inputTransferTo.value
  );
  inputTransferAmount.value = inputTransferTo.value = '';

  if (
    amount > 0 &&
    receiverAcc &&
    currentAccount.balance >= amount &&
    receiverAcc?.username !== currentAccount.username
  ) {
    // Doing the transfer
    currentAccount.movements.push(-amount);
    receiverAcc.movements.push(amount);

    // Add Transfer Date
    currentAccount.movementsDates.push(new Date().toISOString());
    receiverAcc.movementsDates.push(new Date().toISOString());

    // Update UI
    updateUI(currentAccount);

    // Reset the Timer
    clearInterval(timer);
    timer = startlogouttimer();
  }
});

btnLoan.addEventListener('click', function (e) {
  e.preventDefault();

  const amount = Math.floor(inputLoanAmount.value);

  if (amount > 0 && currentAccount.movements.some(mov => mov >= amount * 0.1)) {
    setTimeout(function () {
      // Add movement
      currentAccount.movements.push(amount);

      // Add Transfer Date
      currentAccount.movementsDates.push(new Date().toISOString());

      // Update UI
      updateUI(currentAccount);

      // Reset the Timer
      clearInterval(timer);
      timer = startlogouttimer();
    }, 2500);
  }
  inputLoanAmount.value = '';
});

btnClose.addEventListener('click', function (e) {
  e.preventDefault();

  if (
    inputCloseUsername.value === currentAccount.username &&
    +inputClosePin.value === currentAccount.pin
  ) {
    const index = accounts.findIndex(
      acc => acc.username === currentAccount.username
    );
    console.log(index);
    // .indexOf(23)

    // Delete account
    accounts.splice(index, 1);

    // Hide UI
    containerApp.style.opacity = 0;
  }

  inputCloseUsername.value = inputClosePin.value = '';
});

let sorted = false;
btnSort.addEventListener('click', function (e) {
  e.preventDefault();
  displayMovements(currentAccount, !sorted);
  sorted = !sorted;
});

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// LECTURES

/* CONVERTING AND CHECKING NUMBERS */
// console.log(23 === 23.0);
// console.log(0.1 + 0.3);
// console.log(0.1 + 0.3 == 0.4);

// // Convert String to Number
// console.log(Number('23'));
// console.log(+'23');

// // Parse a Number to String
// console.log(Number.parseInt('2681993@gmail.com')); // 2681993
// console.log(Number.parseInt('ak2681993@gmail.com')); // NaN because we have string before numbers.

// // ParseInt with radix argument
// console.log(Number.parseInt('0px', 2)); // Binary Number System with base 2 => 0,1
// console.log(Number.parseInt('7px', 8)); // Ocatal Number System with base 8 => 0,1,2,3,4,5,6,7
// console.log(Number.parseInt('45px', 10)); // Decimal Number System with base 10 => 0,1,2,3,4,5,6,7,9
// console.log(Number.parseInt('1A3', 16)); // Hexadecimal Number System with base 16 => 0-9,A,B,C,D,E,F

// // ParseFloat
// console.log(Number.parseInt('2.5rem'));
// console.log(Number.parseFloat('2.5rem'));

// // IsNaN
// console.log(Number.isNaN(30)); // False
// console.log(Number.isNaN('Ajay')); // False
// console.log(Number.isNaN(+'Ajay')); // True
// console.log(Number.isNaN(23 / 0)); // False

// // IsFinite()
// console.log(Number.isFinite(30)); // True
// console.log(Number.isFinite('Ajay')); // False
// console.log(Number.isFinite(+'Ajay')); // False
// console.log(Number.isNaN(23 / 0)); // False

// // IsInteger()
// console.log(Number.isInteger(30)); // True
// console.log(Number.isInteger('Ajay')); // False
// console.log(Number.isInteger(48.9)); // False
// console.log(Number.isInteger(48.0)); // True
// console.log(Number.isInteger(23 / 0)); // False

/* MATH and ROUNDING */

// // SquareRoot sqrt()
// console.log('Sqaure Root :-');
// console.log(Math.sqrt(25)); // Gives squareRoot => 5
// console.log(Math.sqrt(25.9)); // Gives squareRoot
// console.log(Math.sqrt('Ajay')); // Gives NaN
// console.log(25 ** (1 / 2)); // Gives square root of 25 => 5, Using exponential
// console.log(25.9 ** (1 / 2));

// // Cubic Root
// console.log('Exponentials :-');
// console.log(27 ** (1 / 3));
// console.log(16 ** (1 / 4));

// // max()
// console.log('max :-');
// const values = [23, 34, 53, 88, 59, 93];
// console.log(Math.max(12, 23, 33, 123, 87));
// console.log(Math.max(...values));

// // min()
// console.log('min :-');
// console.log(Math.min(12, 22, 33, 123, 87));
// console.log(Math.min(...values));

// // PI
// console.log('PI :-');
// const radius = '10px';
// console.log(Math.PI * Number.parseFloat(radius) ** 2);

// // Random()
// console.log('Random :-');
// console.log(Math.random());
// console.log(Math.random() * 6); // Gives the float values upto 6
// console.log(Math.trunc(Math.random() * 6)); // Gives random number upto 5 and to truncate the decimal part

// // trunc()
// console.log('Trunc :-');
// console.log(Math.trunc(39.2323));
// console.log(Math.trunc(Math.random() * 6) + 1); // Gives random numbers exactly from 1-to-6  and to truncate the decimal part

// const randomInt = (min, max) =>
//   Math.floor(Math.random() * (max - min) + 1) + min;
// console.log(randomInt(5, 15));

// ROUNDING INTEGER NUMBERS
// // Round()
// console.log(Math.round(23.4)); // Gives 23, as 23 is closest to 23.4
// console.log(Math.round(23.5));
// console.log(Math.round(23.7)); // Gives 24, as 24 is closest to 23.7

// // Ceil()
// console.log(Math.ceil(23.4)); // 24
// console.log(Math.ceil(23.5)); // 24
// console.log(Math.ceil(23.7)); // 24

// // Floor()
// console.log(Math.floor(23.4)); // 23
// console.log(Math.floor(23.5)); // 23
// console.log(Math.floor(23.7)); // 23

// console.log(Math.trunc(26.9)); // 26
// console.log(Math.trunc(-26.9)); // -26
// console.log(Math.floor(26.9)); // 26
// console.log(Math.floor(-26.9)); // -27

// ROUNDING FLOATING NUMBERS

// // toFixed()
// console.log((27.4).toFixed(0)); // Gives 0 decimal values
// console.log((27.4).toFixed(1)); // Gives 1 decimal values
// console.log((27.4).toFixed(4)); // Gives 4 decimal values

/* THE REMAINDER OPERATOR */

// console.log(5 % 2); // 1 => 5=2*2+1
// console.log(8 % 3); // 2 => 8=2*3+2
// // Code 1
// const nums = [23, 12, 44, 31, 81, 22];
// const numsArr = nums.map(num => (num % 2 === 0 ? 'Even' : 'Odd'));
// console.log(numsArr);
// // Code 2
// const isEven = a => (a % 2 === 0 ? 'Even' : 'Odd');
// console.log(isEven(4));
// console.log(isEven(71));

// // Coloring movements rows to different background color.
// labelBalance.addEventListener('click', function () {
//   [...document.querySelectorAll('.movements__row')].forEach(function (row, i) {
//     if (i % 2 === 0) {
//       row.style.backgroundColor = 'orangered';
//     }
//     if (i % 3 === 0) {
//       row.style.backgroundColor = 'blue';
//     }
//   });
// });

/* NUMERIC SEPERATORS */

// const diameter1 = 287460000000;
// console.log(diameter1);
// const diameter2 = 287_460_000_000;
// console.log(diameter2);

/* WORKING WITH BIGINT */

// console.log(2 ** 53 - 1);
// console.log(9007199254740991);
// console.log(Number.MAX_SAFE_INTEGER);

// console.log(2 ** 53 + 0);
// console.log(2 ** 53 + 1);
// console.log(2 ** 53 + 2);
// console.log(2 ** 53 + 3);
// console.log(2 ** 53 + 4);
// console.log(73667488987376748849957);

// // BigInt
// console.log(73667488987376748849957n);
// console.log(BigInt(73667488987376748849957));

// // Operations with BigInt numbers.
// console.log(10000n + 2000n);
// const num1 = 73667488987376748849957n;
// const num2 = 23;
// // console.log(num1 * num2);
// console.log(10000n > 298); // Greater than operator
// console.log(10000n < 298); // Smaller than operator
// console.log(10000n == '10000'); // Loose equality operator
// console.log(10000n === 10000); // Strict equality operator

// console.log(num1 + ' is really a big number!!');

// console.log(10000n / 2n);
// console.log(10000n / 2);

/* DATES AND TIMES */

// CREATING DATES
// We have 4 ways of creating dates in JS
// const date1 = new Date();
// console.log(date1);
// console.log(new Date('Jan 08 2024 17:41:46'));
// console.log(new Date('December 25 2025'));

// const account3 = {
//   owner: 'Ajay Kumar',
//   movements: [200, 455.23, -306.5, 25000, -642.21, -133.9, 79.97, 1300],
//   interestRate: 1.2, // %
//   pin: 3333,

//   movementsDates: [
//     '2019-11-18T21:31:17.178Z',
//     '2019-12-23T07:42:02.383Z',
//     '2020-01-28T09:15:04.904Z',
//     '2020-04-01T10:17:24.185Z',
//     '2020-05-08T14:11:59.604Z',
//     '2020-05-27T17:01:17.194Z',
//     '2020-07-11T23:36:17.929Z',
//     '2020-07-12T10:51:36.790Z',
//   ],
//   currency: 'EUR',
//   locale: 'pt-PT', // de-DE
// };

// console.log(new Date(account3.movementsDates[0]));
// console.log(new Date(2037, 10, 19, 15, 23, 5));
// console.log(new Date(2032, 10, 35));
// console.log(new Date(0));
// console.log(new Date(3 * 24 * 60 * 60 * 1000));

// Working with dates
// const future = new Date(2037, 10, 19, 15, 23);
// console.log(future);

// // Get full year
// console.log(future.getFullYear());

// // Get month
// console.log(future.getMonth()); // Month in JS is '0' based

// // Get Date
// console.log(future.getDate());

// // Get Day
// console.log(future.getDay());

// // Get Hours
// console.log(future.getHours());

// // Get Minutes
// console.log(future.getMinutes());

// // Get Seconds
// console.log(future.getSeconds());

// // Nice String format of date
// console.log(future.toISOString());

// // Get timestamp
// console.log(future.getTime());
// console.log(new Date(2142237180000));

// // To get current timestamp
// console.log(Date.now());

// // Set Full year of given date
// future.setFullYear(2040);
// future.setMonth(8);
// future.setDate(27);
// future.setHours(23);
// future.setMinutes(45);
// console.log(future);

/* OPERATIONS WITH DATES */

// const future = new Date(2037, 10, 19, 15, 23);
// const future2 = new Date(2037, 10, 23, 15, 23);
// console.log(Number(future));

// const calcDaysPassed = (date1, date2) =>
//   Math.abs(date2 - date1) / (1000 * 60 * 60 * 24);
// const days1 = calcDaysPassed(new Date(2037, 3, 14), new Date(2037, 3, 24));
// console.log(days1);
// const days2 = calcDaysPassed(new Date(2037, 3, 14), new Date(2037, 3, 4));
// console.log(days2);
// const days3 = calcDaysPassed(
//   new Date(2037, 3, 14),
//   new Date(2037, 3, 4, 10, 8)
// );
// console.log(days3);

/* INTERNATIONALIZATION DATES AND NUMBERS */

// // For Dates
// const now = new Date();
// const options = {
//   hour: 'numeric',
//   minute: 'numeric',
//   day: 'numeric',
//   month: 'numeric',
//   year: 'numeric',
//   weekday: 'short',
// };
// const locale = navigator.language;
// console.log(new Intl.DateTimeFormat(locale, options).format(now));

// // For Numbers
// const num = 388823.75667;
// const options = {
//   style: 'currency',
//   unit: 'mile-per-hour',
//   unit: 'celsius',
//   currency: 'EUR',
// };

// console.log('India :- ', new Intl.NumberFormat('en-IN', options).format(num));
// console.log('US :- ', new Intl.NumberFormat('en-US', options).format(num));
// console.log('Germany :- ', new Intl.NumberFormat('de-DE', options).format(num));
// console.log('Russia :- ', new Intl.NumberFormat('ru-RU', options).format(num));
// console.log('Syria :- ', new Intl.NumberFormat('ar-SY', options).format(num));
// console.log(
//   'Local System :- ',
//   new Intl.NumberFormat(navigator.language, options).format(num)
// );

// console.log('US :- ', new Intl.NumberFormat('en-US', options).format(num));

/* TIMERS: setTimeout() and setInterval() */

// // SetTimeOut()
// const ingredients = ['Onion', 'Capsicum', 'Cheese'];
// const pizzaTimer = setTimeout(
//   (ing1, ing2, ing3) =>
//     console.log(
//       `Here is your pizza🍕🍕 with toppings of ${ing1}, ${ing2} and ${ing3}`
//     ),
//   3000,
//   ...ingredients
// ); // This message will display only once after 3 seconds.
// console.log('Waiting......');
// if (ingredients.includes('Olive')) clearTimeout(pizzaTimer);

// SetInterval()

// setInterval(function () {
//   const now = new Date();
//   const options = {
//     hour: 'numeric',
//     minute: 'numeric',
//     second: 'numeric',
//   };
//   console.log(new Intl.DateTimeFormat(navigator.language, options).format(now));
// }, 1000);
