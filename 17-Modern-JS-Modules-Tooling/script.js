/* IMPORT EXPORT */

// // Importing Modules
// console.log('Importing Module');

// import { shippingCost } from './shoppingCart';

// import './shoppingCart.js';

// // 1. Import specific values
// import { addToCart } from './shoppingCart';
// addToCart('Pen', 10);

// 2. Importing multiple at a time.
import {
  addToCart,
  shippingCost,
  totalPrice,
  totalQuantity,
} from './shoppingCart.js';
addToCart('Pen', 10);
console.log(`${shippingCost}, ${totalPrice}, ${totalQuantity}`);

// // 3. Importing with Alias names.
// import { totalPrice as price } from './shoppingCart';
// console.log(`${price}`);

// // 4. Importing All at once using '*'.
// import * as ShoppingCart from './shoppingCart.js';

// // 5. Importing Default with any name.
// import add from './shoppingCart.js';
// add('Pen', 10);

// ShoppingCart.addToCart('Bread', 2);
// console.log(ShoppingCart.shippingCost); // Can't be used because the modules variables in scoped to modules only.
// console.log(`TotalPrice: ${ShoppingCart.totalPrice}Rs/-`);
// console.log(`TotalQuantity: ${ShoppingCart.totalQuantity}`);

/* TOP-LEVEL AWAIT */

// // Simple Promise without Await
// fetch('https://jsonplaceholder.typicode.com/posts')
//   .then(res => res.json())
//   .then(data => console.log(data));

// // Using Await outside the Async function
// const res = await fetch('https://jsonplaceholder.typicode.com/posts');
// const data = await res.json();
// console.log(data);
// console.log('Something after top-level Await'); // This code execute only after the above fetch request completed

// // Problem with top-level Await
// const getLastPost = async function () {
//   const res = await fetch('https://jsonplaceholder.typicode.com/posts');
//   const data = await res.json();
//   return {
//     _id: data.at(-1).id,
//     title: data.at(-1).title,
//     text: data.at(-1).body,
//   };
// };

// // Not very clean
// const lastPost = getLastPost();
// lastPost.then(last => console.log(last)); // Without this we can't print the value of lastPost
// console.log(lastPost);

// // Now use the top-level await for the same above
// const lastPost2 = await getLastPost();
// console.log(lastPost2);

// Importing the top level await from other module

// console.log('hello');
// console.log(shippingCost, cartQuantity);

/* MODULE PATTERN */

// const shoppingCart = (function () {
//   const cart = [];
//   const shippingCost = 20;
//   const totalPrice = 231;
//   const totalQuantity = 23;

//   const addToCart = function (product, quantity) {
//     cart.push({ product, quantity });
//     console.log(`${quantity} "${product}" added to cart!`);
//   };

//   const orderStock = function (product, quantity) {
//     cart.push({ product, quantity });
//     console.log(`${quantity} "${product}" Ordered from supplier!`);
//   };

//   return { cart, totalPrice, totalQuantity, addToCart };
// })();

// shoppingCart.addToCart('pizza', 3);
// console.log(shoppingCart);
// console.log(shoppingCart.shippingCost);

/* COMMON JS MODULE */

// // Export
// export.addToCart= function (product, quantity) {
//     cart.push({ product, quantity });
//     console.log(`${quantity} "${product}" added to cart!`);
//   };

//   // Import
//   const {addToCart}= require('./shoppingCart.js')

/* INTRODUCTION TO NPM */

// Importing the Lodash-es => CloneDeep.js

// import cloneDeep from './node_modules/lodash-es/cloneDeep.js';

// const state = {
//   cart: [
//     { product: 'bread', quantity: 2 },
//     { product: 'pizza', quantity: 4 },
//   ],
//   user: { loggedIn: true },
// };

// // // Copy object simply using JavaScript
// // const stateClone = Object.assign({}, state); // Copy object
// // console.log(stateClone);

// // Copy object using Lodash-es
// const stateClone2 = cloneDeep(state);
// state.user.loggedIn = false; // Change the value of original object
// console.log(stateClone2);
// console.log(state);

/* BUNDLING WITH PARCEL AND NPM SCRIPTS */

import cloneDeep from './node_modules/lodash-es/cloneDeep.js';

const state = {
  cart: [
    { product: 'bread', quantity: 2 },
    { product: 'pizza', quantity: 4 },
  ],
  user: { loggedIn: true },
};

// // Copy object simply using JavaScript
// const stateClone = Object.assign({}, state); // Copy object
// console.log(stateClone);

// Copy object using Lodash-es
const stateClone2 = cloneDeep(state);
state.user.loggedIn = false; // Change the value of original object
console.log(stateClone2);
console.log(state);

if (module.hot) {
  module.hot.accept();
}

/* CONFIGURING BABEL AND POLYFILLING */

class Person {
  #greeting = 'Hey';

  constructor(name) {
    this.name = name;
    console.log(`${this.#greeting} ${this.name}`);
  }
}

const person1 = new Person('Ajay Kumar');

console.log('Jonas' ?? null);

const cart = [
  { product: 'pizza', quantity: 1 },
  { product: 'garlic bread', quantity: 3 },
  { product: 'burger', quantity: 4 },
  { product: 'pizza', quantity: 6 },
  { product: 'burger', quantity: 1 },
];

console.log(cart.find(el => el.quantity >= 2));

Promise.resolve('Test').then(x => console.log(x));

import 'core-js/stable';
// import 'core-js/stable/array/find';
// import 'core-js/stable/promise';

// Polyfilling async function
import 'regenerator-runtime/runtime';
