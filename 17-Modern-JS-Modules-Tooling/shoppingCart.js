/* IMPORT EXPORT */

// // Exporting Modules
// console.log('Exporting Module');

// export const shippingCost = 10;
const cart = [];

const addToCart = function (product, quantity) {
  cart.push({ product, quantity });
  console.log(`${quantity} "${product}" added to cart!`);
};

const totalPrice = 230;
const totalQuantity = 32;
const shippingCost = 100;

// export { addToCart, shippingCost, totalPrice, totalQuantity };

// // 1. Exporting specific values
// export { addToCart };

// 2. Exporting multiple at a time.
export { addToCart, shippingCost, totalPrice, totalQuantity };

// // 3. Exporting with Alias names.
// export { totalPrice as price };

// // 4. Exporting Default
// export default function (product, quantity) {
//   cart.push({ product, quantity });
//   console.log(`${quantity} "${product}" added to cart!`);
// }

// /* BLOCKING CODE FOR THE TOP-LEVEL AWAIT */

// console.log('Starting Fetch Users');
// await fetch('https://jsonplaceholder.typicode.com/users');
// console.log('Finished Fetching Users Details');

// const shippingCost = 10;
// const cartQuantity = 30;
// export { cartQuantity, shippingCost };
