class RecipeView {
  #parentElement = document.querySelector('.recipe');
}

// Exporting the object of class instead of expporting the whole class
export default new RecipeView();
