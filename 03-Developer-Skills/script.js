// CHALLENGE :-
// SOLUTION :-
// PROBLEM :-
// EXAMPLE :-
// TOPIC :-
// CASE :-
// CODE :-
// NOTE :-

// Remember, we're gonna use strict mode in all scripts now!
"use strict";

// TOPIC :-
// Using Google, Stackoverflow, and MDN.

// PROBLEM 1:-
/* We work for a company building a smart home thermometer.
Our most recent task is this:" Given an array of tempratures of one day, 
calculate the temprature amplitude. Keep in mind that sometimes there 
might be a sensor error."*/

const tempratures = [3, -2, -6, -1, "error", 9, 13, 17, 15, 14, 9, 5];

/* 1.) Understand the problem :-
       - What is temprature amplitude?
       Answer.) Difference between heighest and lowest temprature.
       - How to caculate "Max and Min" temprature?
       - What's a sensor error? and what to do?

   2.) Breaking up into sub-problems :-
       - How to ignore 'error'?
       - Find Max value in temprature array?
       - Find Min value in temprature array?
       - Subtract Min from Max and return it?
*/
// SOLUTION :-

let tempAmplitude;

const calcTempAmp = function (temps) {
  let maxTemp = tempratures[0];
  let minTemp = tempratures[0];

  for (let i = 1; i <= temps.length; ++i) {
    if (typeof temps[i] !== "number") continue;

    if (maxTemp < temps[i]) maxTemp = temps[i];

    if (minTemp > temps[i]) minTemp = temps[i];
  }
  console.log(
    `Today's maximum temprature is "${maxTemp}" and minimum temprature is "${minTemp}".`
  );
  return `The temprature amplitude is ${(tempAmplitude = maxTemp - minTemp)}`;
};

console.log(calcTempAmp(tempratures));

// PROBLEM 2:-

// Function should recieve now 2 arrays of temprature.
/* We work for a company building a smart home thermometer.
Our most recent task is this:" Given an array of tempratures of one day, 
calculate the temprature amplitude. Keep in mind that sometimes there 
might be a sensor error."*/

const tempratures2 = [3, -2, -6, -1, "error", 9, 13, 17, 15, 14, 9, 5];
const tempratures3 = [45, -1, 5, 0, 10, 123, 11, -15, -12, -9, -29];
const tempsMerge = tempratures2.concat(tempratures3);

console.log(calcTempAmp(tempsMerge));

// PROBLEM 3:-

// Debugging the given Code
// const measureKelvin = function () {
//   const measurement = {
//     type: "temp",
//     unit: "celcius",
//     value: prompt("Enter the temprature in celcius."),
//   };

//   return measurement.value + 273;
// };
console.error("This is a dummy error message.");
console.warn("This is dummy warning message.");

const measureKelvin = function () {
  const measurement = {
    type: "temp",
    unit: "celcius",
    value: Number(prompt("Enter the temprature in celcius.")),
  };
  console.table(measurement);
  return measurement.value + 273;
};

console.log(measureKelvin());
