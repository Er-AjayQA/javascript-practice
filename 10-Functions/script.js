'use strict';

/* DEFAULT PARAMETERS */

// const bookings = [];
// const createBooking = function (
//   flightNum,
//   numPassangers = 1,
//   price = 200 * numPassangers
// ) {
//   // // ES5 way of setting default values
//   // numPassangers = numPassangers || 1;
//   // price = price || 199;
//   const booking = {
//     flightNum,
//     numPassangers,
//     price,
//   };
//   console.log(booking);
//   bookings.push(booking);
// };
// createBooking('LH12');
// createBooking('LH32', 5, 500);
// createBooking('LH32', 10);
// createBooking('LH32', undefined, 1000); // Skipping the 2nd argument

/* HOW PASSING ARGUMENTS WORKS: VALUES Vs REFERENCE */

// const flight = 'LH12';
// const passenger = {
//   name: 'Ajay Kumar',
//   passportNumber: 19488928343091,
// };

// const checkIn = function (flightNum, passenger) {
//   flightNum = 'LH999';
//   passenger.name = 'Mr.' + passenger.name;
//   if (passenger.passportNumber === 19488928343091) {
//     alert('CheckIn!!');
//   } else {
//     alert('Wrong Passport!!');
//   }
// };

// // checkIn(flight, passenger);
// // console.log(flight);
// // console.log(passenger);

// const newPassport = function (person) {
//   person.passportNumber = Math.trunc(Math.random() * 1000000000000);
// };

// newPassport(passenger);
// checkIn(flight, passenger);

/* FIRST-CLASS FUNCTIONS AND HIGH-ORDER FUNCTIONS */

// (1) First-Class Functions
// (a) All the functions in the js is treated as values, i.e. they can be passed as the arguments to the other functions.
// (b) or they can be stored as values in the variables.

// (2) Higher-Order Functions
// (a) They are the functions that receive the other functions as arguments.
// (b) They are the functions that can return other functions.

/* FUNCTIONS ACCEPTING CALBACK FUNCTION */

// // Example 1
// const oneWord = function (str) {
//   return str.replaceAll(' ', '').toLowerCase();
// };

// const upperFirstWord = function (str) {
//   const [first, ...other] = str.split(' ');
//   return [first.toUpperCase(), ...other].join(' ');
// };

// const upperString = function (str) {
//   return str.toUpperCase();
// };
// // Higher Order function
// const transformer = function (str, fn) {
//   console.log(`Original String: ${str}`);
//   console.log(`Transformed String: ${fn(str)}`);
//   console.log(`Transformed By: ${fn.name}`);
// };

// transformer('Javascript is the best!', upperFirstWord); // Calling function with "upperFirstWord" function as argument
// transformer('Javascript is the best!', oneWord); // Calling function with "oneWord" function as argument
// transformer('Javascript is the best!', upperString); // Calling function with "upperString" function as argument

// // Example 2
// const high5 = function () {
//   console.log('👋');
// };

// document.body.addEventListener('click', high5);

/* FUNCTIONS RETURNING FUNCTIONS */

// // Example 1

// const greet = function (greeting) {
//   return function (name) {
//     console.log(`${greeting} ${name}`);
//   };
// };

// const greeter = greet('Hey!');
// greeter('Ajay Kumar');
// greeter('Komal');
// greet('Hello!!')('Sneha');

// // Example 1 with arrow functions
// const greet = greeting => name => console.log(`${greeting} ${name}`);

// greet('Hello!!')('Ajay Kumar');

/* THE CALL AND APPLY METHODS */

// // Airline 1
// const lufthansa = {
//   airline: 'Lufthansa',
//   iataCode: 'LH',
//   bookings: [],
//   book(flightNum, name) {
//     console.log(
//       `${name} booked a seat on ${this.airline} flight ${this.iataCode}${flightNum}`
//     );
//     this.bookings.push({ flight: `${this.iataCode}${flightNum}`, name });
//   },
// };

// lufthansa.book(320, 'Ajay');
// console.log(lufthansa);

// // Creating the variable for the function "book()"
// const book = lufthansa.book;

// // Airline 2
// const eurowings = {
//   airline: 'Eurowings',
//   iataCode: 'EW',
//   bookings: [],
//   book, // using the function "book()" value here.
// };

// // Airline 3
// const swiss = {
//   airline: 'Swiss',
//   iataCode: 'SW',
//   bookings: [],
//   book, // using the function "book()" value here.
// };

// // Problem :- This code not work now because now the 'this' keyword is undefined, as the book() is independent method not..
// // book(2324, 'Vijay');

// const flightData1 = [521, 'Maxwell'];

// // Solution 1 :- By using the call() method.
// book.call(eurowings, 321, 'Vijay'); // Calling "call()" method which will call the "book()" method with "this" keyword set to "eurowings"
// book.call(lufthansa, 777, 'Aarav'); // Calling "call()" method which will call the "book()" method with "this" keyword set to "lufthansa"
// book.call(swiss, 888, 'Aarushi'); // Calling "call()" method which will call the "book()" method with "this" keyword set to "Swiss"
// book.call(lufthansa, ...flightData1);

// // Solution 2 :- By using the apply() method.
// const flightData2 = [543, 'George Cooper'];
// book.apply(swiss, flightData2);

/* THE BIND METHOD */

// // Airline 1
// const lufthansa = {
//   airline: 'Lufthansa',
//   iataCode: 'LH',
//   bookings: [],
//   book(flightNum, name) {
//     console.log(
//       `${name} booked a seat on ${this.airline} flight ${this.iataCode}${flightNum}`
//     );
//     this.bookings.push({ flight: `${this.iataCode}${flightNum}`, name });
//   },
// };

// // Creating the variable for the function "book()"
// const book = lufthansa.book;

// // Airline 2
// const eurowings = {
//   airline: 'Eurowings',
//   iataCode: 'EW',
//   bookings: [],
//   book, // using the function "book()" value here.
// };

// // bind() method => It will create the new method instead of calling the existing method.
// const bookEW = book.bind(eurowings); // Creating the new book method for eurowing.
// bookEW(989, 'Vinay'); // Calling the new method.

// book.bind(lufthansa, 11)('kaushik');
// book.bind(lufthansa)(484, 'krrish'); // We can call the method like this also but each time it will create the new method leads to memory wastage

// // Setting the default values of function arguments by using the "bind()" methods
// const bookEW23 = book.bind(eurowings, 23); // Here "flightNum" will always set to "23".
// bookEW23('Tanish');
// bookEW23('Martha');

// // With EventListeners
// lufthansa.planes = 300;
// lufthansa.buyPlanes = function () {
//   console.log(this);
//   this.planes++;
//   console.log(this.planes);
// };

// // document.querySelector('.buy').addEventListener('click', lufthansa.buyPlanes); // In eventHandler the "this" keyword will always point to the element to which the Handler is attached.
// document
//   .querySelector('.buy')
//   .addEventListener('click', lufthansa.buyPlanes.bind(lufthansa)); // Now the "this" keyword will point to the object "lufthansa"

// // Example => Partial Application
// const addTax = (rate, value) => (value + value) * rate;
// console.log(addTax(0.1, 200));

// const addVAT = addTax.bind(null, 0.23); //  Storing the "addRate()" function with the default value of "rate=0.23" to "addVAT" variable.
// // addVAT = (value) => value + value * 0.23; // Above function expression is equal to this.
// console.log(addVAT(400));

// // Challenege 1 => Above function with return function

// const addTAX = function (rate, value) {
//   const tax = (value + value) * rate;
//   console.log(tax);
//   return addTAX.bind(null, 0.23);
// };
// addTAX(0.23, 200);

// const addVAT = addTAX(2, 400);
// addVAT(400);

// // Challenege 2 => Above function with return function
// const addTAX = function (rate) {
//   return function (value) {
//     return (value + value) * rate;
//   };
// };
// const addVAT = addTAX(0.23);
// console.log(addVAT(300));

/* IMMEDIATELY INVOKED FUNCTION EXPRESSIONS */

// const runOnce = function () {
//   console.log('This will never run again.');
// };
// runOnce();

// // For regular function expression
// (function () {
//   console.log('Function runs automatically.');
//   const isPrivate = 23;
// })();

// // console.log(isPrivate); // Can't access this variable as it has not any scope outside the function

// // For arrow functions
// (() => console.log('Automatic arrow function'))();

// // Why we use IIFE
// {
//   const isPrivate = 'Private';
//   var notPrivate = 'Not Private';
// }
// console.log(notPrivate); // Can be accessed outside the block as it not follow any scope
// console.log(isPrivate); // Can't be accessed outside the block as it follow any scope

/* CLOSURES */

// // Closure Situation-1
// const secureBooking = function () {
//   let passengerCount = 0;

//   return function () {
//     passengerCount++;
//     console.log(`${passengerCount} Passengers`);
//   };
// };

// // console.log(passengerCount); //Can't access "passengerCount" because it has the function scope only

// const booking = secureBooking();
// booking();
// console.dir(booking);
// booking();
// booking();

// Closure Situation-2
